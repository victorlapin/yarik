package com.victorlapin.yarik

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.room.Room
import androidx.room.testing.MigrationTestHelper
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory
import androidx.test.platform.app.InstrumentationRegistry
import com.victorlapin.yarik.model.database.AppDatabase
import com.victorlapin.yarik.model.database.entity.Source
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

/**
 * Created by victor on 20.03.2020.
 */
class V1to2 {
    @Rule
    @JvmField
    val testHelper = MigrationTestHelper(
        InstrumentationRegistry.getInstrumentation(),
        AppDatabase::class.java.canonicalName,
        FrameworkSQLiteOpenHelperFactory()
    )

    private fun getMigratedDatabase(): AppDatabase {
        val database = Room.databaseBuilder(
                InstrumentationRegistry.getInstrumentation().targetContext,
                AppDatabase::class.java,
                DatabaseTestSuite.TEST_DB_NAME
            )
            .allowMainThreadQueries()
            .addMigrations(AppDatabase.MIGRATION_1_2)
            .build()
        testHelper.closeWhenFinished(database)
        return database
    }

    @Test
    fun migrate_12() {
        val db = testHelper.createDatabase(DatabaseTestSuite.TEST_DB_NAME, 1)
        val cv = ContentValues()
        cv.put("id", 1L)
        cv.put("name", "Test Feed")
        cv.put("url", "https://some.feed")
        cv.put("is_enabled", true)
        db.insert("sources", SQLiteDatabase.CONFLICT_REPLACE, cv)
        db.close()

        testHelper.runMigrationsAndValidate(
            DatabaseTestSuite.TEST_DB_NAME, 2,
            false, AppDatabase.MIGRATION_1_2
        )
        val dao = getMigratedDatabase().getTestsDao()

        val testSource = Source(
            id = 1,
            name = "Test Feed",
            url = "https://some.feed",
            isEnabled = true,
            lastBuildDate = 0,
            updatePeriod = "everytime"
        )
        val source = dao.testGetSource(1)

        assertEquals(testSource.name, source.name)
        assertEquals(testSource.url, source.url)
        assertEquals(testSource.isEnabled, source.isEnabled)
        assertEquals(testSource.updatePeriod, source.updatePeriod)
        assertEquals(testSource.lastBuildDate, source.lastBuildDate)
    }
}