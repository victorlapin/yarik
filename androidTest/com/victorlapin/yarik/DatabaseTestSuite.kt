package com.victorlapin.yarik

/**
 * Created by victor on 20.03.2020.
 */
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    V1to2::class
)
class DatabaseTestSuite {
    companion object {
        const val TEST_DB_NAME = "yarik_test.db"
    }
}