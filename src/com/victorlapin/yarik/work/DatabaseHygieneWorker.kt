package com.victorlapin.yarik.work

import android.content.Context
import androidx.work.Constraints
import androidx.work.CoroutineWorker
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkerParameters
import com.victorlapin.yarik.model.interactor.ArticlesInteractor
import com.victorlapin.yarik.provider.SettingsProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.concurrent.TimeUnit

class DatabaseHygieneWorker(
    context: Context,
    params: WorkerParameters,
    private val interactor: ArticlesInteractor,
    private val settings: SettingsProvider
) : CoroutineWorker(context, params) {

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            Timber.tag(WORK_TAG).i("Worker started")

            val date = System.currentTimeMillis() -
                    TimeUnit.DAYS.toMillis(settings.daysToKeep.toLong())
            val rows = interactor.deleteOldArticles(date)
            Timber.tag(WORK_TAG).i("Rows deleted: $rows")
            interactor.vacuum()

            Result.success()
        } catch (ex: Exception) {
            Timber.tag(WORK_TAG).e(ex)
            Result.retry()
        } finally {
            Timber.tag(WORK_TAG).i("Worker finished")
        }
    }

    companion object {
        const val WORK_TAG = "DatabaseHygieneWorker"

        fun buildRequest(): PeriodicWorkRequest {
            val constraints = Constraints.Builder()
                .setRequiresBatteryNotLow(true)
                .setRequiresDeviceIdle(true)
                .build()

            return PeriodicWorkRequest.Builder(
                DatabaseHygieneWorker::class.java,
                1L, TimeUnit.DAYS
            )
                .setConstraints(constraints)
                .build()
        }
    }
}