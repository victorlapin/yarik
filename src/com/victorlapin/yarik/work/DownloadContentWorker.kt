package com.victorlapin.yarik.work

import android.content.Context
import androidx.work.*
import com.victorlapin.yarik.model.interactor.FeedInteractor
import com.victorlapin.yarik.provider.ServicesProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.concurrent.TimeUnit

class DownloadContentWorker(
    context: Context,
    params: WorkerParameters,
    private val feedInteractor: FeedInteractor,
    private val services: ServicesProvider
) : CoroutineWorker(context, params) {

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            Timber.tag(WORK_TAG).i("Worker started")

            val newArticlesCount = feedInteractor.refreshFeed(this)
            if (newArticlesCount > 0) {
                services.showNewArticlesNotification(newArticlesCount)
            }

            Result.success()
        } catch (ex: Exception) {
            Timber.tag(WORK_TAG).e(ex)
            Result.failure()
        } finally {
            Timber.tag(WORK_TAG).i("Worker finished")
        }
    }

    companion object {
        const val WORK_TAG = "DownloadContentWorker"

        fun buildRequest(isWifiOnly: Boolean): PeriodicWorkRequest {
            val constraints = Constraints.Builder()
                .setRequiresBatteryNotLow(true)
                .setRequiredNetworkType(
                    if (isWifiOnly) NetworkType.UNMETERED else NetworkType.CONNECTED
                )
                .build()

            return PeriodicWorkRequest.Builder(
                DownloadContentWorker::class.java,
                1L, TimeUnit.HOURS
            )
                .setConstraints(constraints)
                .build()
        }
    }
}