package com.victorlapin.yarik.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkerParameters
import com.victorlapin.yarik.model.interactor.BlacklistInteractor
import com.victorlapin.yarik.model.interactor.FirebaseDbInteractor
import com.victorlapin.yarik.model.interactor.SourcesInteractor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.concurrent.TimeUnit

class DeferredSyncWorker(
    context: Context,
    params: WorkerParameters,
    private val sourcesInteractor: SourcesInteractor,
    private val blacklistInteractor: BlacklistInteractor,
    private val firebaseInteractor: FirebaseDbInteractor
) : CoroutineWorker(context, params) {

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            Timber.tag(WORK_TAG).i("Worker started")

            val sources = sourcesInteractor.getSourcesImmediate()
            val blacklist = blacklistInteractor.getBlacklistImmediate()
            firebaseInteractor.upsertSources(sources)
            firebaseInteractor.upsertBlacklist(blacklist)

            Result.success()
        } catch (ex: Exception) {
            Timber.tag(WORK_TAG).e(ex)
            Result.retry()
        } finally {
            Timber.tag(WORK_TAG).i("Worker finished")
        }
    }

    companion object {
        const val WORK_TAG = "DeferredSyncWorker"

        fun buildRequest() = OneTimeWorkRequest
            .Builder(DeferredSyncWorker::class.java)
            .setInitialDelay(5L, TimeUnit.MINUTES)
            .build()
    }
}