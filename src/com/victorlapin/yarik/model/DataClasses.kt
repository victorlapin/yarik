package com.victorlapin.yarik.model

import androidx.annotation.StringRes

data class SnackbarInfo(
    @StringRes val messageId: Int? = null,
    val message: String? = null,
    val formatArg: Any? = null
)