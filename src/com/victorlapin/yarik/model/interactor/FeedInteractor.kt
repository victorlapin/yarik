package com.victorlapin.yarik.model.interactor

import com.victorlapin.yarik.model.NoSourcesException
import com.victorlapin.yarik.model.database.entity.ArticleWithSource
import com.victorlapin.yarik.model.database.entity.Source
import com.victorlapin.yarik.model.repository.ArticlesRepository
import com.victorlapin.yarik.model.repository.RssRepository
import com.victorlapin.yarik.model.repository.SourcesRepository
import com.victorlapin.yarik.provider.SettingsProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import timber.log.Timber
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

class FeedInteractor(
    private val sourcesRepo: SourcesRepository,
    private val rssRepo: RssRepository,
    private val articlesRepo: ArticlesRepository,
    private val settings: SettingsProvider
) {
    @Throws(NoSourcesException::class)
    suspend fun refreshFeed(scope: CoroutineScope): Int {
        val sdf = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
        val sourcesRaw = sourcesRepo.getSourcesImmediate().filter { it.isEnabled }
        val sources = mutableListOf<Source>()
        if (settings.useOptimizedFetch) {
            sourcesRaw.forEach {
                val nextBuildTime = when (it.updatePeriod) {
                    "hourly" -> it.lastBuildDate + TimeUnit.HOURS.toMillis(1)
                    else -> it.lastBuildDate
                }
                Timber.d(
                    "Source '${it.name}' last built at ${sdf.format(it.lastBuildDate)}, next update time is ${sdf.format(
                        nextBuildTime
                    )}"
                )
                if (nextBuildTime < System.currentTimeMillis()) {
                    Timber.d("Source '${it.name}' is to be fetched")
                    sources.add(it)
                }
            }
        } else {
            sources.addAll(sourcesRaw)
        }

        if (sources.isNullOrEmpty()) {
            throw NoSourcesException()
        } else {
            val keepTime = System.currentTimeMillis() -
                    TimeUnit.DAYS.toMillis(settings.daysToKeep.toLong())
            val defers = ArrayList<Deferred<List<ArticleWithSource>>>()
            val articles = ArrayList<ArticleWithSource>()
            sources.forEach {
                defers.add(scope.async(scope.coroutineContext) {
                    rssRepo.fetchSource(it, keepTime)
                })
            }
            defers.forEach {
                articles.addAll(it.await())
            }

            sourcesRepo.updateSources(sources)
            val ids = articlesRepo.insertArticles(articles)
            return articlesRepo.getFilteredArticlesCount(ids)
        }
    }
}