package com.victorlapin.yarik.model.interactor

import com.victorlapin.yarik.model.repository.WorkerRepository

class WorkerInteractor(
    private val repo: WorkerRepository
) {
    fun scheduleDatabaseHygieneWorker() =
        repo.scheduleDatabaseHygieneWorker()

    fun scheduleDownloadContentWorker() =
        repo.scheduleDownloadContentWorker()

    fun cancelDownloadContentWorker() =
        repo.cancelDownloadContentWorker()

    fun scheduleDeferredSyncWorker() =
        repo.scheduleDeferredSyncWorker()
}