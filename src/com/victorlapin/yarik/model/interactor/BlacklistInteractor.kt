package com.victorlapin.yarik.model.interactor

import androidx.lifecycle.LiveData
import com.victorlapin.yarik.model.database.entity.BlacklistItem
import com.victorlapin.yarik.model.repository.BlacklistRepository
import com.victorlapin.yarik.model.repository.FirebaseDbRepository

class BlacklistInteractor(
    private val repo: BlacklistRepository,
    private val firebaseRepo: FirebaseDbRepository
) {
    fun getBlacklist(): LiveData<List<BlacklistItem>> = repo.getBlacklist()

    suspend fun getBlacklistImmediate(): List<BlacklistItem> = repo.getBlacklistImmediate()

    suspend fun getTypedBlacklist(type: String): List<BlacklistItem> =
        repo.getTypedBlacklist(type)

    suspend fun insertBlacklistItem(item: BlacklistItem) {
        repo.insertBlacklistItem(item)
        firebaseRepo.upsertBlacklistItem(item)
    }

    suspend fun insertBlacklistItems(items: List<BlacklistItem>) {
        repo.insertBlacklistItems(items)
        items.forEach { item ->
            firebaseRepo.upsertBlacklistItem(item)
        }
    }

    suspend fun deleteBlacklistItem(item: BlacklistItem) {
        repo.deleteBlacklistItem(item)
        firebaseRepo.deleteBlacklistItem(item)
    }
}