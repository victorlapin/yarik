package com.victorlapin.yarik.model.interactor

import androidx.lifecycle.LiveData
import com.victorlapin.yarik.model.database.entity.Article
import com.victorlapin.yarik.model.database.entity.ArticleWithSource
import com.victorlapin.yarik.model.repository.ArticlesRepository

class ArticlesInteractor(
    private val repo: ArticlesRepository
) {
    fun getArticles(): LiveData<List<ArticleWithSource>> = repo.getArticles()

    suspend fun deleteOldArticles(date: Long): Int = repo.deleteOldArticles(date)

    suspend fun vacuum() = repo.vacuum()

    fun updateArticle(article: Article) = repo.updateArticle(article)

    fun getUnreadArticles() = repo.getUnreadArticles()
}