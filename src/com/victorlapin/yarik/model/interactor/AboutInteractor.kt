package com.victorlapin.yarik.model.interactor

import com.victorlapin.yarik.model.repository.AboutRepository

class AboutInteractor(private val repo: AboutRepository) {
    fun getData() = repo.getData()
}