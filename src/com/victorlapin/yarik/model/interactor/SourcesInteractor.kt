package com.victorlapin.yarik.model.interactor

import androidx.lifecycle.LiveData
import com.victorlapin.yarik.model.database.entity.Source
import com.victorlapin.yarik.model.repository.FirebaseDbRepository
import com.victorlapin.yarik.model.repository.SourcesRepository

class SourcesInteractor(
    private val repo: SourcesRepository,
    private val firebaseRepo: FirebaseDbRepository
) {
    fun getSources(): LiveData<List<Source>> = repo.getSources()

    suspend fun getSourcesImmediate() = repo.getSourcesImmediate()

    suspend fun insertSource(source: Source) {
        repo.insertSource(source)
        firebaseRepo.upsertSource(source)
    }

    suspend fun updateSource(source: Source) {
        repo.updateSource(source)
        firebaseRepo.upsertSource(source)
    }

    suspend fun deleteSource(source: Source) {
        repo.deleteSource(source)
        firebaseRepo.deleteSource(source)
    }
}