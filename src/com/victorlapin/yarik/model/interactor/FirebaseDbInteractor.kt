package com.victorlapin.yarik.model.interactor

import com.google.firebase.auth.FirebaseUser
import com.victorlapin.yarik.model.database.entity.BlacklistItem
import com.victorlapin.yarik.model.database.entity.Source
import com.victorlapin.yarik.model.repository.FirebaseDbRepository

class FirebaseDbInteractor(
    private val repo: FirebaseDbRepository
) {
    fun setUser(user: FirebaseUser?) = repo.setUser(user)

    fun upsertSources(sources: List<Source>) {
        sources.forEach { repo.upsertSource(it) }
    }

    fun upsertBlacklist(items: List<BlacklistItem>) {
        items.forEach { repo.upsertBlacklistItem(it) }
    }
}