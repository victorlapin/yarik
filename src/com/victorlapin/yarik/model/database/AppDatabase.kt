package com.victorlapin.yarik.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.victorlapin.yarik.model.database.dao.ArticlesDao
import com.victorlapin.yarik.model.database.dao.BlacklistDao
import com.victorlapin.yarik.model.database.dao.SourcesDao
import com.victorlapin.yarik.model.database.dao.TestsDao
import com.victorlapin.yarik.model.database.entity.Article
import com.victorlapin.yarik.model.database.entity.BlacklistItem
import com.victorlapin.yarik.model.database.entity.Source
import com.victorlapin.yarik.model.database.entity.Tag

@Database(
    entities = [
        Source::class,
        Article::class,
        BlacklistItem::class,
        Tag::class
    ],
    version = 2,
    exportSchema = true
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getSourcesDao(): SourcesDao
    abstract fun getArticlesDao(): ArticlesDao
    abstract fun getBlacklistDao(): BlacklistDao
    abstract fun getTestsDao(): TestsDao

    companion object {
        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL(
                    "CREATE TABLE `sources_temp` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `url` TEXT NOT NULL, `is_enabled` INTEGER NOT NULL, `last_build` INTEGER NOT NULL, `update_period` TEXT NOT NULL)"
                )
                db.execSQL(
                    "insert into sources_temp (id, name, url, is_enabled, last_build, update_period) select id, name, url, is_enabled, 0, 'everytime' from sources"
                )
                db.execSQL("drop table sources")
                db.execSQL("alter table sources_temp rename to sources")
                db.execSQL("CREATE UNIQUE INDEX index_sources_url ON sources (url)")
            }
        }
    }
}