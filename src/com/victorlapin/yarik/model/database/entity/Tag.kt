package com.victorlapin.yarik.model.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(
    tableName = "article_tags",
    indices = [
        Index(value = ["value"]),
        Index(value = ["article_id", "value"], unique = true)
    ],
    foreignKeys = [
        ForeignKey(
            entity = Article::class,
            parentColumns = ["id"],
            childColumns = ["article_id"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Tag(
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    @ColumnInfo(name = "article_id")
    var articleId: Long? = null,
    val value: String
) : Serializable {
    override fun toString(): String = value
}