package com.victorlapin.yarik.model.database.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "blacklist",
    indices = [
        Index(value = ["value"]),
        Index(value = ["value", "type"], unique = true)
    ]
)
data class BlacklistItem(
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    val value: String,
    val type: Int
) {
    fun cloudId(): String = "${type}__${value.replace("/", "_").replace(" ", "")}"

    fun toMap(): HashMap<String, Any> = HashMap<String, Any>().apply {
        this["value"] = this@BlacklistItem.value
        this["type"] = this@BlacklistItem.type
    }

    companion object {
        const val TYPE_AUTHOR = 0
        const val TYPE_TAG = 1

        fun fromMap(map: Map<String, Any>): BlacklistItem = BlacklistItem(
            value = map["value"].toString(),
            type = map["type"].toString().toInt()
        )
    }
}