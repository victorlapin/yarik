package com.victorlapin.yarik.model.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.net.URI

@Entity(
    tableName = "sources",
    indices = [Index(value = ["url"], unique = true)]
)
data class Source(
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    var name: String,
    val url: String,
    @ColumnInfo(name = "is_enabled")
    var isEnabled: Boolean = true,
    @ColumnInfo(name = "last_build")
    var lastBuildDate: Long = 0,
    @ColumnInfo(name = "update_period")
    var updatePeriod: String = "everytime"
) {
    fun cloudId(): String = URI.create(url).host

    fun toMap(): HashMap<String, Any> = HashMap<String, Any>().apply {
        this["name"] = this@Source.name
        this["url"] = this@Source.url
        this["is_enabled"] = this@Source.isEnabled
        this["last_build"] = this@Source.lastBuildDate
        this["update_period"] = this@Source.updatePeriod
    }

    companion object {
        fun fromMap(map: Map<String, Any>): Source = Source(
            name = map["name"].toString(),
            url = map["url"].toString(),
            isEnabled = map["is_enabled"] as Boolean,
            lastBuildDate = if (map["last_build"] != null) map["last_build"] as Long else 0,
            updatePeriod = if (map["update_period"] != null) map["update_period"].toString() else "everytime"
        )
    }
}