package com.victorlapin.yarik.model.database.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.io.Serializable

@Entity(
    tableName = "articles",
    indices = [
        Index(value = ["source_id"]),
        Index(value = ["source_id", "title", "author"], unique = true)
    ],
    foreignKeys = [
        ForeignKey(
            entity = Source::class,
            parentColumns = ["id"],
            childColumns = ["source_id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Article(
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    @ColumnInfo(name = "source_id")
    val sourceId: Long? = null,
    val title: String? = null,
    val author: String? = null,
    val link: String? = null,
    @ColumnInfo(name = "pub_date")
    val pubDate: Long? = null,
    val description: String? = null,
    val image: String? = null,
    @ColumnInfo(name = "is_new")
    var isNew: Boolean = true,
    val content: String? = null
) : Serializable

data class ArticleWithSource(
    @Embedded
    val article: Article,
    val sourceName: String,
    @Relation(parentColumn = "id", entityColumn = "article_id")
    val tags: List<Tag>
) : Serializable