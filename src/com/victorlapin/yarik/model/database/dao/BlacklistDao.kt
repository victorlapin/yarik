package com.victorlapin.yarik.model.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.victorlapin.yarik.model.database.entity.BlacklistItem

@Dao
interface BlacklistDao {
    @Query("select * from blacklist")
    fun getBlacklist(): LiveData<List<BlacklistItem>>

    @Query("select * from blacklist")
    suspend fun getBlacklistImmediate(): List<BlacklistItem>

    @Query("select * from blacklist where type = :type")
    suspend fun getTypedBlacklist(type: String): List<BlacklistItem>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(item: BlacklistItem): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(items: List<BlacklistItem>)

    @Delete
    suspend fun delete(item: BlacklistItem)
}