package com.victorlapin.yarik.model.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.victorlapin.yarik.model.database.entity.Source

/**
 * Created by victor on 20.03.2020.
 */
@Dao
interface TestsDao {
    @Query("select * from sources where id = :id")
    fun testGetSource(id: Long): Source
}