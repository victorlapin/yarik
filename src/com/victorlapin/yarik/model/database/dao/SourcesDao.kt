package com.victorlapin.yarik.model.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.victorlapin.yarik.model.database.entity.Source

@Dao
interface SourcesDao {
    @Query("select * from sources")
    fun getSources(): LiveData<List<Source>>

    @Query("select * from sources")
    suspend fun getSourcesImmediate(): List<Source>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(source: Source): Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(source: Source)

    @Delete
    suspend fun delete(source: Source)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(sources: List<Source>)
}