package com.victorlapin.yarik.model.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.RawQuery
import androidx.room.Transaction
import androidx.room.Update
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.sqlite.db.SupportSQLiteQuery
import com.victorlapin.yarik.model.database.entity.Article
import com.victorlapin.yarik.model.database.entity.ArticleWithSource
import com.victorlapin.yarik.model.database.entity.Tag

@Dao
interface ArticlesDao {
    @Transaction
    @Query(
        "select a.*, s.name as sourceName " +
                "from articles a " +
                "left join (" +
                "    select distinct at.article_id as id " +
                "    from article_tags at " +
                "    join blacklist b2 on at.value = b2.value and b2.type = 1" +
                ") " +
                "  ex on a.id = ex.id " +
                "join sources s on a.source_id = s.id " +
                "left join blacklist b on b.value = a.author and b.type = 0 " +
                "where s.is_enabled = 1 and b.id is null and ex.id is null " +
                "order by a.pub_date desc"
    )
    fun getArticles(): LiveData<List<ArticleWithSource>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(article: Article): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTags(tags: List<Tag>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(article: Article)

    @Query("delete from articles where pub_date < :date")
    suspend fun deleteOldArticles(date: Long): Int

    @RawQuery
    suspend fun vacuum(query: SupportSQLiteQuery = SimpleSQLiteQuery("vacuum")): Int

    @Transaction
    fun upsert(articles: List<ArticleWithSource>): List<Long> {
        val insertedRows = ArrayList<Long>()
        articles.forEach {
            val id = insert(it.article)
            if (id != -1L) {
                insertedRows.add(id)
                it.tags.forEach { tag -> tag.articleId = id }
                insertTags(it.tags)
            } else {
                update(it.article)
                insertTags(it.tags)
            }
        }
        return insertedRows
    }

    @Transaction
    @Query(
        "select count(*) " +
                "from articles a " +
                "left join (" +
                "    select distinct at.article_id as id " +
                "    from article_tags at " +
                "    join blacklist b2 on at.value = b2.value and b2.type = 1" +
                ") " +
                "  ex on a.id = ex.id " +
                "join sources s on a.source_id = s.id " +
                "left join blacklist b on b.value = a.author and b.type = 0 " +
                "where a.id in (:ids) and s.is_enabled = 1 and b.id is null and ex.id is null"
    )
    suspend fun getFilteredArticlesCount(ids: List<Long>): Int

    @Query(
        "select a.* " +
                "from articles a " +
                "left join (" +
                "    select distinct at.article_id as id " +
                "    from article_tags at " +
                "    join blacklist b2 on at.value = b2.value and b2.type = 1" +
                ") " +
                "  ex on a.id = ex.id " +
                "join sources s on a.source_id = s.id " +
                "left join blacklist b on b.value = a.author and b.type = 0 " +
                "where s.is_enabled = 1 and b.id is null and ex.id is null and a.is_new = 1"
    )
    fun getUnreadArticles(): LiveData<List<Article>>
}