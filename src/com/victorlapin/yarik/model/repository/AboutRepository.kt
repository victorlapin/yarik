package com.victorlapin.yarik.model.repository

import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.google.android.gms.common.GoogleApiAvailability
import com.victorlapin.yarik.R
import com.victorlapin.yarik.provider.ResourcesProvider
import com.victorlapin.yarik.provider.ServicesProvider

class AboutRepository(
    private val services: ServicesProvider,
    private val resources: ResourcesProvider
) {
    private val colorGenerator = ColorGenerator.MATERIAL
    private val builder = TextDrawable.builder()
        .beginConfig()
        .textColor(resources.getColor(android.R.color.white))
        .fontSize(48)
        .bold()
        .toUpperCase()
        .endConfig()
        .round()

    fun getData(): List<ListItem> {
        val result = ArrayList<ListItem>()

        // show versions
        result.add(ListItem(name = ITEM_VERSIONS))
        // the app itself
        result.add(getAppInfo(services.selfPackageName))
        // play services
        result.add(getAppInfo(GoogleApiAvailability.GOOGLE_PLAY_SERVICES_PACKAGE))

        // show developers
        result.add(ListItem(name = ITEM_TEAM))
        result.add(
            getCreditItem(
                nameRes = R.string.about_team_victor,
                descriptionRes = R.string.about_developer
            )
        )

        // show links
//        result.add(ListItem(name = ITEM_LINKS))
//        result.add(
//            getLinkItem(
//                nameRes = R.string.about_links_source_code,
//                descriptionRes = R.string.about_links_gitlab,
//                imageRes = R.drawable.git,
//                screen = AboutExternalScreen("https://gitlab.com/victorlapin/usage-stats")
//            )
//        )
//        result.add(
//            getLinkItem(
//                nameRes = R.string.about_links_oss,
//                descriptionRes = R.string.about_links_oss_text,
//                imageRes = R.drawable.library,
//                screen = OssScreen()
//            )
//        )

        // show credits
        result.add(ListItem(name = ITEM_CREDITS))
        result.add(
            getCreditItem(
                nameRes = R.string.about_credits_marco,
                descriptionRes = R.string.about_credits_marco_text
            )
        )
        result.add(
            getCreditItem(
                nameRes = R.string.about_credits_philippe,
                descriptionRes = R.string.about_credits_philippe_text
            )
        )
        result.add(
            getCreditItem(
                nameRes = R.string.about_credits_austin,
                descriptionRes = R.string.about_credits_austin_text
            )
        )

        return result
    }

    private fun getAppInfo(packageName: String): ListItem {
        val item = ListItem("")
        try {
            val pm = services.packageManager
            val packageInfo = pm.getPackageInfo(packageName, 0)
            item.name = pm.getApplicationLabel(packageInfo.applicationInfo)
            item.image = packageInfo.applicationInfo.loadIcon(pm)
            item.description = packageInfo.versionName
        } catch (ex: PackageManager.NameNotFoundException) {
            ex.printStackTrace()
            item.name = packageName
            item.description = resources.getString(R.string.not_found).uppercase()
            item.isError = true
        }

        return item
    }

    private fun getLinkItem(
        @StringRes nameRes: Int,
        @StringRes descriptionRes: Int,
        @DrawableRes imageRes: Int
    ): ListItem =
        ListItem(
            name = resources.getString(nameRes),
            description = resources.getString(descriptionRes),
            image = resources.getDrawable(imageRes)
        )

    private fun getCreditItem(
        @StringRes nameRes: Int,
        @StringRes descriptionRes: Int
    ): ListItem {
        val item = ListItem(
            name = resources.getString(nameRes),
            description = resources.getString(descriptionRes)
        )
        item.image = builder.build(
            item.name.filter { it.isUpperCase() }.toString(),
            colorGenerator.getColor(item.name)
        )
        return item
    }

    data class ListItem(
        var name: CharSequence,
        var description: CharSequence? = null,
        var image: Drawable? = null,
        var isError: Boolean = false,
        var url: CharSequence? = null
    )

    companion object {
        const val ITEM_VERSIONS = "ITEM_VERSIONS"
        const val ITEM_TEAM = "ITEM_TEAM"
        const val ITEM_CREDITS = "ITEM_CREDITS"
        const val ITEM_LINKS = "ITEM_LINKS"
    }
}