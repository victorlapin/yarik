package com.victorlapin.yarik.model.repository

import androidx.lifecycle.LiveData
import com.victorlapin.yarik.model.database.dao.BlacklistDao
import com.victorlapin.yarik.model.database.entity.BlacklistItem

class BlacklistRepository(
    private val dao: BlacklistDao
) {
    fun getBlacklist(): LiveData<List<BlacklistItem>> = dao.getBlacklist()

    suspend fun getTypedBlacklist(type: String): List<BlacklistItem> =
        dao.getTypedBlacklist(type)

    suspend fun getBlacklistImmediate() = dao.getBlacklistImmediate()

    suspend fun insertBlacklistItem(item: BlacklistItem) = dao.insert(item)

    suspend fun insertBlacklistItems(items: List<BlacklistItem>) = dao.insert(items)

    suspend fun deleteBlacklistItem(item: BlacklistItem) = dao.delete(item)
}