package com.victorlapin.yarik.model.repository

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.QuerySnapshot
import com.victorlapin.yarik.BuildConfig
import com.victorlapin.yarik.model.database.dao.BlacklistDao
import com.victorlapin.yarik.model.database.dao.SourcesDao
import com.victorlapin.yarik.model.database.entity.BlacklistItem
import com.victorlapin.yarik.model.database.entity.Source
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import timber.log.Timber

class FirebaseDbRepository(
    private val sourcesDao: SourcesDao,
    private val blacklistDao: BlacklistDao
) {
    private var sourcesRef: CollectionReference? = null
    private var blacklistRef: CollectionReference? = null
    private var sourcesRegistration: ListenerRegistration? = null
    private var blacklistRegistration: ListenerRegistration? = null
    private var scope: CoroutineScope? = null

    init {
        FirebaseFirestore.setLoggingEnabled(BuildConfig.DEBUG)
    }

    fun setUser(user: FirebaseUser?) {
        Timber.tag(TAG).d("Setting user: ${user?.email}")
        if (user != null) {
            scope = CoroutineScope(Dispatchers.IO)
            sourcesRef = FirebaseFirestore.getInstance()
                .collection("users")
                .document(user.uid)
                .collection("sources")
            blacklistRef = FirebaseFirestore.getInstance()
                .collection("users")
                .document(user.uid)
                .collection("blacklist")

            sourcesRegistration = sourcesRef?.addSnapshotListener(mSourcesListener)
            blacklistRegistration = blacklistRef?.addSnapshotListener(mBlacklistListener)
        } else {
            sourcesRegistration?.remove()
            blacklistRegistration?.remove()

            sourcesRef = null
            blacklistRef = null

            scope?.coroutineContext?.cancel()
            scope = null
        }
    }

    fun upsertSource(source: Source) {
        sourcesRef
            ?.document(source.cloudId())
            ?.set(source.toMap())
    }

    fun deleteSource(source: Source) {
        sourcesRef
            ?.document(source.cloudId())
            ?.delete()
    }

    fun upsertBlacklistItem(item: BlacklistItem) {
        blacklistRef
            ?.document(item.cloudId())
            ?.set(item.toMap())
    }

    fun deleteBlacklistItem(item: BlacklistItem) {
        blacklistRef
            ?.document(item.cloudId())
            ?.delete()
    }

    private val mSourcesListener = EventListener<QuerySnapshot> { snapshots, e ->
        if (e != null) {
            Timber.tag(TAG).e(e)
            return@EventListener
        }

        if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
            for (dc in snapshots.documentChanges) {
                val source = Source.fromMap(dc.document.data)
                when (dc.type) {
                    DocumentChange.Type.ADDED -> scope?.launch { sourcesDao.insert(source) }
                    DocumentChange.Type.MODIFIED -> scope?.launch { sourcesDao.update(source) }
                    DocumentChange.Type.REMOVED -> scope?.launch { sourcesDao.delete(source) }
                }
            }
        }
    }

    private val mBlacklistListener = EventListener<QuerySnapshot> { snapshots, e ->
        if (e != null) {
            Timber.tag(TAG).e(e)
            return@EventListener
        }

        if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
            for (dc in snapshots.documentChanges) {
                val item = BlacklistItem.fromMap(dc.document.data)
                when (dc.type) {
                    DocumentChange.Type.ADDED -> scope?.launch { blacklistDao.insert(item) }
                    DocumentChange.Type.MODIFIED -> {
                    }
                    DocumentChange.Type.REMOVED -> scope?.launch { blacklistDao.delete(item) }
                }
            }
        }
    }

    companion object {
        private const val TAG = "FirebaseDbRepository"
    }
}