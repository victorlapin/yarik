package com.victorlapin.yarik.model.repository

import com.prof.rssparser.Parser
import com.victorlapin.yarik.model.database.entity.Article
import com.victorlapin.yarik.model.database.entity.ArticleWithSource
import com.victorlapin.yarik.model.database.entity.Source
import com.victorlapin.yarik.model.database.entity.Tag
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class RssRepository(
    private val parser: Parser
) {
    suspend fun fetchSource(source: Source, keepTime: Long): List<ArticleWithSource> {
        try {
            Timber.d("Source requested: ${source.name}")
            val result = ArrayList<ArticleWithSource>()
            val sdf = SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH)
            val channel = parser.getChannel(source.url)
            channel.articles.forEach {
                if (it.pubDate != null) {
                    val pubDate = sdf.parse(it.pubDate!!)?.time ?: 0
                    if (pubDate >= keepTime) {
                        val tags = ArrayList<Tag>()
                        tags.addAll(it.categories.map { c -> Tag(value = c.trim()) })

                        val article = Article(
                            sourceId = source.id,
                            title = it.title,
                            author = it.author,
                            link = it.link,
                            pubDate = pubDate,
                            description = it.description,
                            image = it.image,
                            content = it.content
                        )

                        result.add(
                            ArticleWithSource(
                                article = article,
                                sourceName = source.name,
                                tags = tags
                            )
                        )
                    }
                }
            }

            channel.lastBuildDate?.let {
                val buildDate = sdf.parse(it)?.time ?: 0
                source.lastBuildDate = buildDate
            }
            channel.updatePeriod?.let { source.updatePeriod = it }

            Timber.d("Source finished: ${source.name}, articles: ${result.size}")
            return result
        } catch (e: Exception) {
            Timber.tag(TAG)
                .e(e, "Error fetching source '${source.name}', url: ${source.url}")
            return emptyList()
        }
    }

    companion object {
        private const val TAG = "RssRepository"
    }
}