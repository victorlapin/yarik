package com.victorlapin.yarik.model.repository

import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.ExistingWorkPolicy
import androidx.work.WorkManager
import com.victorlapin.yarik.provider.SettingsProvider
import com.victorlapin.yarik.work.DatabaseHygieneWorker
import com.victorlapin.yarik.work.DeferredSyncWorker
import com.victorlapin.yarik.work.DownloadContentWorker
import timber.log.Timber

class WorkerRepository(
    private val workManager: WorkManager,
    private val settings: SettingsProvider
) {
    companion object {
        private const val TAG = "WorkerRepository"
    }

    fun scheduleDatabaseHygieneWorker() {
        workManager.enqueueUniquePeriodicWork(
            DatabaseHygieneWorker.WORK_TAG,
            ExistingPeriodicWorkPolicy.KEEP,
            DatabaseHygieneWorker.buildRequest()
        )
        Timber.tag(TAG).i("Database hygiene worker scheduled")
    }

    fun scheduleDownloadContentWorker() {
        workManager.enqueueUniquePeriodicWork(
            DownloadContentWorker.WORK_TAG,
            ExistingPeriodicWorkPolicy.REPLACE,
            DownloadContentWorker.buildRequest(settings.downloadViaWifiOnly)
        )
        Timber.tag(TAG).i("Download content worker scheduled")
    }

    fun cancelDownloadContentWorker() {
        workManager.cancelUniqueWork(DownloadContentWorker.WORK_TAG)
        Timber.tag(TAG).i("Download content worker canceled")
    }

    fun scheduleDeferredSyncWorker() {
        workManager.enqueueUniqueWork(
            DeferredSyncWorker.WORK_TAG,
            ExistingWorkPolicy.REPLACE,
            DeferredSyncWorker.buildRequest()
        )
        Timber.tag(TAG).i("Deferred sync worker scheduled")
    }
}