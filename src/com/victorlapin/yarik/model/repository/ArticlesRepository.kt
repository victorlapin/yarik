package com.victorlapin.yarik.model.repository

import androidx.lifecycle.LiveData
import com.victorlapin.yarik.model.database.dao.ArticlesDao
import com.victorlapin.yarik.model.database.entity.Article
import com.victorlapin.yarik.model.database.entity.ArticleWithSource

class ArticlesRepository(
    private val dao: ArticlesDao
) {
    fun getArticles(): LiveData<List<ArticleWithSource>> = dao.getArticles()

    fun insertArticles(articles: List<ArticleWithSource>): List<Long> =
        dao.upsert(articles)

    suspend fun deleteOldArticles(date: Long): Int = dao.deleteOldArticles(date)

    suspend fun vacuum() = dao.vacuum()

    suspend fun getFilteredArticlesCount(ids: List<Long>): Int =
        dao.getFilteredArticlesCount(ids)

    fun updateArticle(article: Article) = dao.update(article)

    fun getUnreadArticles() = dao.getUnreadArticles()
}