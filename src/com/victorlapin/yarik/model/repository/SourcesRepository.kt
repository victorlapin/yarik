package com.victorlapin.yarik.model.repository

import androidx.lifecycle.LiveData
import com.victorlapin.yarik.model.database.dao.SourcesDao
import com.victorlapin.yarik.model.database.entity.Source

class SourcesRepository(
    private val dao: SourcesDao
) {
    fun getSources(): LiveData<List<Source>> = dao.getSources()

    suspend fun getSourcesImmediate() = dao.getSourcesImmediate()

    suspend fun insertSource(source: Source) = dao.insert(source)

    suspend fun updateSource(source: Source) = dao.update(source)

    suspend fun deleteSource(source: Source) = dao.delete(source)

    suspend fun updateSources(sources: List<Source>) = dao.update(sources)
}