package com.victorlapin.yarik.di

val allModules = listOf(
    appModule,
    modelModule,
    uiModule,
    networkModule,
    workModule
)
