package com.victorlapin.yarik.di

import androidx.room.Room
import androidx.work.WorkManager
import com.prof.rssparser.Parser
import com.victorlapin.yarik.model.database.AppDatabase
import com.victorlapin.yarik.model.interactor.AboutInteractor
import com.victorlapin.yarik.model.interactor.ArticlesInteractor
import com.victorlapin.yarik.model.interactor.BlacklistInteractor
import com.victorlapin.yarik.model.interactor.FeedInteractor
import com.victorlapin.yarik.model.interactor.FirebaseDbInteractor
import com.victorlapin.yarik.model.interactor.SourcesInteractor
import com.victorlapin.yarik.model.interactor.WorkerInteractor
import com.victorlapin.yarik.model.repository.AboutRepository
import com.victorlapin.yarik.model.repository.ArticlesRepository
import com.victorlapin.yarik.model.repository.BlacklistRepository
import com.victorlapin.yarik.model.repository.FirebaseDbRepository
import com.victorlapin.yarik.model.repository.RssRepository
import com.victorlapin.yarik.model.repository.SourcesRepository
import com.victorlapin.yarik.model.repository.WorkerRepository
import org.koin.dsl.module

val modelModule = module {
    single {
        Room.databaseBuilder(get(), AppDatabase::class.java, "news.db")
            .addMigrations(AppDatabase.MIGRATION_1_2)
            .build()
    }
    single { get<AppDatabase>().getSourcesDao() }
    single { get<AppDatabase>().getArticlesDao() }
    single { get<AppDatabase>().getBlacklistDao() }

    single { SourcesRepository(get()) }
    single { SourcesInteractor(get(), get()) }
    single { ArticlesRepository(get()) }
    single { ArticlesInteractor(get()) }
    single { RssRepository(get()) }
    single { Parser.Builder(okHttpClient = get()).build() }
    single { FeedInteractor(get(), get(), get(), get()) }
    single { WorkManager.getInstance(get()) }
    single { WorkerRepository(get(), get()) }
    single { WorkerInteractor(get()) }
    single { BlacklistRepository(get()) }
    single { BlacklistInteractor(get(), get()) }
    single { FirebaseDbRepository(get(), get()) }
    single { FirebaseDbInteractor(get()) }
    single { AboutRepository(get(), get()) }
    single { AboutInteractor(get()) }
}