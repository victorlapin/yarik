package com.victorlapin.yarik.di

import com.victorlapin.yarik.viewmodel.AboutViewModel
import com.victorlapin.yarik.viewmodel.BlacklistViewModel
import com.victorlapin.yarik.viewmodel.FeedViewModel
import com.victorlapin.yarik.viewmodel.MainViewModel
import com.victorlapin.yarik.viewmodel.ShareReceiverViewModel
import com.victorlapin.yarik.viewmodel.SourcesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val uiModule = module {
    viewModel { MainViewModel(get(), get(), get(), get()) }
    viewModel { SourcesViewModel(get()) }
    viewModel { FeedViewModel(get(), get(), get(), get()) }
    viewModel { BlacklistViewModel(get()) }
    viewModel { ShareReceiverViewModel(get()) }
    viewModel { AboutViewModel(get()) }
}