package com.victorlapin.yarik.di

import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.victorlapin.yarik.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import java.util.concurrent.TimeUnit

val networkModule = module {
    single {
        with(OkHttpClient.Builder()) {
            connectTimeout(30, TimeUnit.SECONDS)
            readTimeout(30, TimeUnit.SECONDS)
            if (BuildConfig.DEBUG) {
                addNetworkInterceptor(
                    HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
                )
                addInterceptor(ChuckerInterceptor.Builder(get()).build())
            }
            build()
        }
    }
}