package com.victorlapin.yarik.di

import com.victorlapin.yarik.work.DatabaseHygieneWorker
import com.victorlapin.yarik.work.DeferredSyncWorker
import com.victorlapin.yarik.work.DownloadContentWorker
import org.koin.androidx.workmanager.dsl.worker
import org.koin.dsl.module

/**
 * Created by victor on 23.03.2021
 */
val workModule = module {
    worker { DatabaseHygieneWorker(get(), get(), get(), get()) }
    worker { DeferredSyncWorker(get(), get(), get(), get(), get()) }
    worker { DownloadContentWorker(get(), get(), get(), get()) }
}