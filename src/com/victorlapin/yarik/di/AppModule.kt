package com.victorlapin.yarik.di

import android.content.Context
import com.victorlapin.yarik.provider.CustomTabsProvider
import com.victorlapin.yarik.provider.ResourcesProvider
import com.victorlapin.yarik.provider.ServicesProvider
import com.victorlapin.yarik.provider.SettingsProvider
import org.koin.dsl.module

val appModule = module {
    single { SettingsProvider(get()) }
    single { ResourcesProvider(get()) }
    single { ServicesProvider(get(), get()) }
    single { CustomTabsProvider(get<Context>().packageName) }
}