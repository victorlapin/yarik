package com.victorlapin.yarik

import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding

const val EDGE_TO_EDGE_FLAGS = (View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)

class ViewState {
    var margins = Rect()
    var padding = Rect()
}

fun View.setEdgeToEdgeSystemUiFlags(enabled: Boolean) {
    systemUiVisibility = systemUiVisibility and EDGE_TO_EDGE_FLAGS.inv() or
            if (enabled) EDGE_TO_EDGE_FLAGS else 0
}

fun View.addSystemTopPadding(
    targetView: View = this,
    isConsumed: Boolean = false
) {
    doOnApplyWindowInsets { _, insets, initialState ->
        val systemInsets = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        targetView.updatePadding(
            top = initialState.padding.top + systemInsets.top
        )
        if (isConsumed) {
            WindowInsetsCompat.Builder(insets)
                .setInsets(
                    WindowInsetsCompat.Type.systemBars(),
                    Insets.of(
                        systemInsets.left,
                        0,
                        systemInsets.right,
                        systemInsets.bottom
                    )
                ).build()
        } else {
            insets
        }
    }
}

fun View.addSystemBottomPadding(
    targetView: View = this,
    isConsumed: Boolean = false
) {
    doOnApplyWindowInsets { _, insets, initialState ->
        val systemInsets = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        targetView.updatePadding(
            bottom = initialState.padding.bottom + systemInsets.bottom
        )
        if (isConsumed) {
            WindowInsetsCompat.Builder(insets)
                .setInsets(
                    WindowInsetsCompat.Type.systemBars(),
                    Insets.of(
                        systemInsets.left,
                        systemInsets.top,
                        systemInsets.right,
                        0
                    )
                ).build()
        } else {
            insets
        }
    }
}

fun View.addSystemTopMargin(
    targetView: View = this,
    isConsumed: Boolean = false
) {
    doOnApplyWindowInsets { _, insets, initialState ->
        if (targetView.layoutParams is ViewGroup.MarginLayoutParams) {
            val systemInsets = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            targetView.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                topMargin = initialState.margins.top + systemInsets.top
            }
            if (isConsumed) {
                WindowInsetsCompat.Builder(insets)
                    .setInsets(
                        WindowInsetsCompat.Type.systemBars(),
                        Insets.of(
                            systemInsets.left,
                            0,
                            systemInsets.right,
                            systemInsets.bottom
                        )
                    ).build()
            } else {
                insets
            }
        } else {
            insets
        }
    }
}

fun View.addSystemBottomMargin(
    targetView: View = this,
    isConsumed: Boolean = false
) {
    doOnApplyWindowInsets { _, insets, initialState ->
        if (targetView.layoutParams is ViewGroup.MarginLayoutParams) {
            val systemInsets = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            targetView.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                bottomMargin = initialState.margins.bottom + systemInsets.bottom
            }
            if (isConsumed) {
                WindowInsetsCompat.Builder(insets)
                    .setInsets(
                        WindowInsetsCompat.Type.systemBars(),
                        Insets.of(
                            systemInsets.left,
                            systemInsets.top,
                            systemInsets.right,
                            0
                        )
                    ).build()
            } else {
                insets
            }
        } else {
            insets
        }
    }
}

fun View.doOnApplyWindowInsets(
    targetView: View = this,
    block: (View, insets: WindowInsetsCompat, initialState: ViewState) -> WindowInsetsCompat
) {
    val initialState = ViewState().apply {
        padding = recordInitialPaddingForView(targetView)
        margins = recordInitialMarginsForView(targetView)
    }
    ViewCompat.setOnApplyWindowInsetsListener(targetView) { v, insets ->
        block(v, insets, initialState)
    }
    requestApplyInsetsWhenAttached()
}

private fun recordInitialPaddingForView(view: View) =
    Rect(view.paddingLeft, view.paddingTop, view.paddingRight, view.paddingBottom)

private fun recordInitialMarginsForView(view: View): Rect =
    if (view.layoutParams is ViewGroup.MarginLayoutParams) {
        val params = view.layoutParams as ViewGroup.MarginLayoutParams
        Rect(params.leftMargin, params.topMargin, params.rightMargin, params.bottomMargin)
    } else {
        Rect()
    }

private fun View.requestApplyInsetsWhenAttached() {
    if (isAttachedToWindow) {
        ViewCompat.requestApplyInsets(this)
    } else {
        addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                v.removeOnAttachStateChangeListener(this)
                ViewCompat.requestApplyInsets(v)
            }

            override fun onViewDetachedFromWindow(v: View) = Unit
        })
    }
}
