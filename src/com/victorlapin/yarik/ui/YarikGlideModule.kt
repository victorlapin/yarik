package com.victorlapin.yarik.ui

import android.content.Context
import android.util.Log
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory
import com.bumptech.glide.module.AppGlideModule
import com.victorlapin.yarik.BuildConfig

@GlideModule
class YarikGlideModule : AppGlideModule() {
    override fun applyOptions(context: Context, builder: GlideBuilder) {
        val cacheSize: Long = 1024 * 1024 * 150 // 150 MB
        builder.setDiskCache(InternalCacheDiskCacheFactory(context, "hero_images", cacheSize))
        if (BuildConfig.DEBUG) {
            builder.setLogLevel(Log.VERBOSE)
        }
    }
}