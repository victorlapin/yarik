package com.victorlapin.yarik.ui.activities

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.annotation.StyleRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.victorlapin.yarik.provider.SettingsProvider
import com.victorlapin.yarik.setEdgeToEdgeSystemUiFlags
import org.koin.android.ext.android.inject

abstract class BaseActivity : AppCompatActivity(), LifecycleObserver {
    private val settings by inject<SettingsProvider>()

    @StyleRes
    private var mCurrentTheme: Int = 0
    private val mHandler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        mCurrentTheme = settings.theme
        setTheme(mCurrentTheme)
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(this)
        val root = window.decorView.findViewById<View>(android.R.id.content)
        root.setEdgeToEdgeSystemUiFlags(enabled = true)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun checkTheme() {
        val newTheme = settings.theme
        if (mCurrentTheme != newTheme) {
            mHandler.post { updateTheme(newTheme) }
        }
    }

    fun updateTheme(@StyleRes newTheme: Int) {
        mCurrentTheme = newTheme
        recreate()
    }
}