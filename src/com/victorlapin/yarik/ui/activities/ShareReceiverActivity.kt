package com.victorlapin.yarik.ui.activities

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.callbacks.onDismiss
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.victorlapin.yarik.R
import com.victorlapin.yarik.databinding.DialogEditSourceBinding
import com.victorlapin.yarik.model.database.entity.Source
import com.victorlapin.yarik.provider.SettingsProvider
import com.victorlapin.yarik.viewmodel.ShareReceiverViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ShareReceiverActivity : AppCompatActivity() {
    private val viewModel by viewModel<ShareReceiverViewModel>()
    private val settings by inject<SettingsProvider>()

    override fun onCreate(savedInstanceState: Bundle?) {
        val theme = when (settings.theme) {
            R.style.AppTheme_Light_Pixel -> R.style.AppTheme_Transparent_Light_Pixel
            else -> R.style.AppTheme_Transparent_Dark_Pixel
        }
        setTheme(theme)
        super.onCreate(savedInstanceState)

        showNewSourceDialog()
    }

    private fun showNewSourceDialog() {
        val intentReader = ShareCompat.IntentReader(this)
        if (intentReader.isSingleShare) {
            val sharedUrl = intentReader.text
            val dialog = MaterialDialog(this)
                .lifecycleOwner(this)
                .title(res = R.string.source_new)
                .customView(viewRes = R.layout.dialog_edit_source, scrollable = true)
                .onDismiss { finish() }
                .negativeButton(res = android.R.string.cancel)
                .positiveButton(res = android.R.string.ok) { dialog ->
                    val view = dialog.getCustomView()
                    with(DialogEditSourceBinding.bind(view)) {
                        val name = edtName.text.toString()
                        val url = edtUrl.text.toString()
                        when {
                            name.isBlank() -> edtName.error =
                                getString(R.string.source_error_empty)
                            url.isBlank() -> edtUrl.error =
                                getString(R.string.source_error_empty)
                            else -> {
                                val newSource = Source(
                                    name = name.trim(),
                                    url = url.trim()
                                )
                                viewModel.onSourceAdded(newSource)
                            }
                        }
                    }
                }

            val view = dialog.getCustomView()
            with(DialogEditSourceBinding.bind(view)) {
                edtUrl.setText(sharedUrl, TextView.BufferType.EDITABLE)
                edtName.requestFocus()
            }
            dialog.show()
        } else {
            finish()
        }
    }
}