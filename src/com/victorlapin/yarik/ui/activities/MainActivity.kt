package com.victorlapin.yarik.ui.activities

import android.content.ComponentName
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.ViewGroup
import androidx.browser.customtabs.CustomTabsClient
import androidx.browser.customtabs.CustomTabsServiceConnection
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.victorlapin.yarik.R
import com.victorlapin.yarik.addSystemBottomPadding
import com.victorlapin.yarik.databinding.ActivityMainBinding
import com.victorlapin.yarik.doOnApplyWindowInsets
import com.victorlapin.yarik.provider.CustomTabsProvider
import com.victorlapin.yarik.ui.fragments.Scrollable
import com.victorlapin.yarik.viewmodel.MainViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel by viewModel<MainViewModel>()
    private val customTabs by inject<CustomTabsProvider>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun setControls() {
        viewModel.onStarted()
        with(binding) {
            val navController = findNavController(R.id.fragment_container)
            navigation.apply {
                setupWithNavController(navController)
                setOnItemReselectedListener {
                    val fragment = supportFragmentManager
                        .findFragmentById(R.id.fragment_container)
                        ?.childFragmentManager
                        ?.primaryNavigationFragment
                    if (fragment is Scrollable) {
                        fragment.scrollToTop()
                    }
                }
            }
            navController.addOnDestinationChangedListener { _, dest, _ ->
                when (dest.id) {
                    R.id.fragment_about -> navigation.isVisible = false
                    else -> navigation.isVisible = true
                }
            }

            viewModel.newArticlesCountData.observe(this@MainActivity) {
                reportNewArticlesCount(it)
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun setTransitions() {
        window.apply {
            exitTransition = TransitionInflater.from(this@MainActivity)
                .inflateTransition(R.transition.activity_main_exit)
            reenterTransition = TransitionInflater.from(this@MainActivity)
                .inflateTransition(R.transition.activity_main_reenter)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun applyInsets() {
        with(binding) {
            navigation.addSystemBottomPadding(isConsumed = true)
            container.doOnApplyWindowInsets { view, insets, initialState ->
                val systemInsets = insets.getInsets(WindowInsetsCompat.Type.systemBars())
                view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                    leftMargin = initialState.margins.left + systemInsets.left
                    rightMargin = initialState.margins.right + systemInsets.right
                }
                insets
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.fragment_container)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    private val chromeConnection = object : CustomTabsServiceConnection() {
        override fun onCustomTabsServiceConnected(name: ComponentName, client: CustomTabsClient) {
            customTabs.setClient(client)
            this@MainActivity.unbindService(this)
        }

        override fun onServiceDisconnected(name: ComponentName?) {

        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun setCustomTabs() {
        val chromePackage = CustomTabsClient.getPackageName(this, null)
        Timber.tag("CustomTabs").i("Package found: $chromePackage")
        chromePackage?.let {
            CustomTabsClient.bindCustomTabsService(this, it, chromeConnection)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun finalizeCustomTabs() = customTabs.cleanup()

    private fun reportNewArticlesCount(count: Int) {
        with(binding) {
            if (count > 0) {
                navigation.getOrCreateBadge(R.id.fragment_feed).apply {
                    number = count
                    isVisible = true
                    badgeTextColor = getColor(android.R.color.white)
                    backgroundColor = getColor(R.color.badge_background)
                }
            } else {
                navigation.getBadge(R.id.fragment_feed)?.isVisible = false
            }
        }
    }
}