package com.victorlapin.yarik.ui.activities

import android.os.Bundle
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ImageSpan
import android.text.style.URLSpan
import android.transition.TransitionInflater
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.core.text.getSpans
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import androidx.navigation.navArgs
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.victorlapin.yarik.R
import com.victorlapin.yarik.databinding.ActivityReadArticleBinding
import com.victorlapin.yarik.doOnApplyWindowInsets
import com.victorlapin.yarik.provider.CustomTabsProvider
import com.victorlapin.yarik.provider.SettingsProvider
import com.victorlapin.yarik.ui.ImageGetter
import me.saket.bettermovementmethod.BetterLinkMovementMethod
import org.koin.android.ext.android.inject
import org.sufficientlysecure.htmltextview.HtmlFormatter
import org.sufficientlysecure.htmltextview.HtmlFormatterBuilder
import timber.log.Timber

class ArticleActivity : BaseActivity() {

    private lateinit var binding: ActivityReadArticleBinding
    private val args by navArgs<ArticleActivityArgs>()
    private val customTabs by inject<CustomTabsProvider>()
    private val settings by inject<SettingsProvider>()

    @ColorInt
    private var windowBackgroundColor: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReadArticleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val array =
            theme.obtainStyledAttributes(intArrayOf(android.R.attr.windowBackground))
        windowBackgroundColor = array.getColor(0, 0x000000)
        array.recycle()

        with(binding) {
            lblTitle.text = args.article.title
            toolbar.setNavigationOnClickListener { onBackPressed() }
            toolbar.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_font_size -> showFontSizeDialog()
                }
                true
            }
            setFontSize()

            val builder = HtmlFormatterBuilder().apply {
                html = args.article.content ?: args.article.description
                isRemoveTrailingWhiteSpace = false
                if (settings.showImages) {
                    imageGetter = ImageGetter(lblContent)
                }
            }
            val contentText = try {
                HtmlFormatter.formatHtml(builder) as Spannable
            } catch (ignore: Exception) {
                Timber.e(ignore)
                SpannableString(
                    Html.fromHtml(
                        builder.html,
                        0,
                        if (settings.showImages) {
                            ImageGetter(lblContent)
                        } else null,
                        null
                    )
                )
            }
            contentText.getSpans<ImageSpan>().forEach {
                val start = contentText.getSpanStart(it)
                val end = contentText.getSpanEnd(it)
                contentText.setSpan(
                    URLSpan(it.source),
                    start,
                    end,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
            lblContent.text = contentText
            val linkMovement =
                BetterLinkMovementMethod.newInstance().setOnLinkClickListener { _, url ->
                    openUrl(url)
                    true
                }
            lblContent.movementMethod = linkMovement

            args.article.link?.let { url ->
                if (settings.useCustomTabs && settings.preloadArticles) {
                    customTabs.preloadUrl(url)
                }
                fab.setOnClickListener { openUrl(url) }
            }

            scrollView.setOnScrollChangeListener { _: NestedScrollView?, _: Int, scrollY: Int, _: Int, _: Int ->
                if (scrollY == 0) {
                    if (!fab.isExtended) {
                        fab.extend()
                    }
                } else {
                    if (fab.isExtended) {
                        fab.shrink()
                    }
                }
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun setTransitions() {
        window.apply {
            enterTransition = TransitionInflater.from(this@ArticleActivity)
                .inflateTransition(R.transition.activity_article_enter)
            sharedElementEnterTransition = TransitionInflater.from(this@ArticleActivity)
                .inflateTransition(R.transition.activity_article_se_enter)
            returnTransition = TransitionInflater.from(this@ArticleActivity)
                .inflateTransition(R.transition.activity_article_return)
            sharedElementReturnTransition = TransitionInflater.from(this@ArticleActivity)
                .inflateTransition(R.transition.activity_article_se_return)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun applyInsets() {
        with(binding) {
            fab.doOnApplyWindowInsets { view, insets, initialState ->
                val systemInsets = insets.getInsets(WindowInsetsCompat.Type.systemBars())
                view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                    bottomMargin = initialState.margins.bottom + systemInsets.bottom
                    rightMargin = initialState.margins.right + systemInsets.right
                }
                insets
            }
            scrollView.doOnApplyWindowInsets { view, insets, initialState ->
                val systemInsets = insets.getInsets(WindowInsetsCompat.Type.systemBars())
                view.updatePadding(
                    top = initialState.padding.top + systemInsets.top
                )
                view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                    leftMargin = initialState.margins.left + systemInsets.left
                    rightMargin = initialState.margins.right + systemInsets.right
                }
                insets
            }
        }
    }

    private fun setFontSize() {
        with(binding) {
            when (settings.fontSize) {
                SettingsProvider.FONT_SIZE_SMALL ->
                    lblContent.setTextAppearance(R.style.TextAppearance_MaterialComponents_Body2)
                SettingsProvider.FONT_SIZE_MEDIUM ->
                    lblContent.setTextAppearance(R.style.TextAppearance_MaterialComponents_Body1)
                SettingsProvider.FONT_SIZE_LARGE ->
                    lblContent.setTextAppearance(R.style.TextAppearance_MaterialComponents_Headline6)
            }
        }
    }

    private fun showFontSizeDialog() {
        val entries = listOf(
            getString(R.string.font_size_small),
            getString(R.string.font_size_medium),
            getString(R.string.font_size_large)
        )

        MaterialDialog(this).show {
            lifecycleOwner(this@ArticleActivity)
            title(R.string.action_font_size)
            listItemsSingleChoice(
                items = entries,
                initialSelection = settings.fontSize,
                waitForPositiveButton = false
            ) { dialog, index, _ ->
                dialog.dismiss()
                settings.fontSize = index
                setFontSize()
            }
            negativeButton(android.R.string.cancel)
        }
    }

    private fun openUrl(url: String) {
        customTabs.openUrl(
            this,
            url,
            windowBackgroundColor!!,
            settings.useCustomTabs,
            settings.theme != R.style.AppTheme_Light_Pixel
        )
    }
}