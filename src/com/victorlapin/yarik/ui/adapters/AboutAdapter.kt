package com.victorlapin.yarik.ui.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.victorlapin.yarik.R
import com.victorlapin.yarik.inflate
import com.victorlapin.yarik.model.repository.AboutRepository
import com.victorlapin.yarik.provider.ResourcesProvider

class AboutAdapter(
    resources: ResourcesProvider
) : RecyclerView.Adapter<AboutAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val items = ArrayList<AboutRepository.ListItem>()

    private val strVersions = resources.getString(R.string.about_versions)
    private val strTeam = resources.getString(R.string.about_the_team)
    private val strCredits = resources.getString(R.string.about_credits)
    private val strLinks = resources.getString(R.string.about_links)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = when (viewType) {
            ITEM_TYPE_VERSIONS, ITEM_TYPE_TEAM, ITEM_TYPE_CREDITS, ITEM_TYPE_LINKS ->
                parent.inflate(R.layout.item_about_category)
            else -> parent.inflate(R.layout.item_about)
        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            ITEM_TYPE_VERSIONS -> holder.itemView.findViewById<TextView>(R.id.lbl_name).text =
                strVersions
            ITEM_TYPE_TEAM -> holder.itemView.findViewById<TextView>(R.id.lbl_name).text = strTeam
            ITEM_TYPE_CREDITS -> holder.itemView.findViewById<TextView>(R.id.lbl_name).text =
                strCredits
            ITEM_TYPE_LINKS -> holder.itemView.findViewById<TextView>(R.id.lbl_name).text = strLinks
            ITEM_TYPE_ITEM -> {
                val item = items[position]
                holder.itemView.findViewById<ViewGroup>(R.id.click_container)
                    .setOnClickListener(null)
                holder.itemView.findViewById<TextView>(R.id.lbl_name).text = item.name
                holder.itemView.findViewById<TextView>(R.id.lbl_description).text = item.description
                holder.itemView.findViewById<ImageView>(R.id.img_picture)
                    .setImageDrawable(item.image)
            }
        }
    }

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return when (item.name) {
            AboutRepository.ITEM_TEAM -> ITEM_TYPE_TEAM
            AboutRepository.ITEM_VERSIONS -> ITEM_TYPE_VERSIONS
            AboutRepository.ITEM_CREDITS -> ITEM_TYPE_CREDITS
            AboutRepository.ITEM_LINKS -> ITEM_TYPE_LINKS
            else -> ITEM_TYPE_ITEM
        }
    }

    fun setData(data: List<AboutRepository.ListItem>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

    companion object {
        const val ITEM_TYPE_ITEM = 0
        private const val ITEM_TYPE_VERSIONS = 1
        private const val ITEM_TYPE_TEAM = 2
        private const val ITEM_TYPE_CREDITS = 3
        private const val ITEM_TYPE_LINKS = 4
    }
}