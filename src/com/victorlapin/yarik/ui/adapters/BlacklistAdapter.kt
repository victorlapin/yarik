package com.victorlapin.yarik.ui.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.victorlapin.yarik.R
import com.victorlapin.yarik.databinding.ItemBlacklistBinding
import com.victorlapin.yarik.inflate
import com.victorlapin.yarik.model.database.entity.BlacklistItem

class BlacklistAdapter(
    private val itemDeleteListener: (BlacklistItem) -> Unit = { }
) : ListAdapter<BlacklistItem, BlacklistAdapter.ViewHolder>(Callback()) {
    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = currentList[position]
        with(ItemBlacklistBinding.bind(holder.itemView)) {
            lblValue.text = item.value
            btnDelete.setOnClickListener { itemDeleteListener(item) }
            imgType.setImageResource(
                when (item.type) {
                    BlacklistItem.TYPE_AUTHOR -> R.drawable.account_outline
                    BlacklistItem.TYPE_TAG -> R.drawable.tag_outline
                    else -> R.drawable.cancel
                }
            )
        }
    }

    override fun getItemId(position: Int): Long = currentList[position].id!!

    inner class ViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_blacklist))

    private class Callback : DiffUtil.ItemCallback<BlacklistItem>() {
        override fun areItemsTheSame(oldItem: BlacklistItem, newItem: BlacklistItem): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: BlacklistItem, newItem: BlacklistItem): Boolean {
            return oldItem.type == newItem.type &&
                    oldItem.value == newItem.value
        }
    }
}