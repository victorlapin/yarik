package com.victorlapin.yarik.ui.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.victorlapin.yarik.R
import com.victorlapin.yarik.databinding.ItemSourceBinding
import com.victorlapin.yarik.inflate
import com.victorlapin.yarik.model.database.entity.Source

class SourcesAdapter(
    private val itemClickListener: (Source) -> Unit = { },
    private val itemDeleteListener: (Source) -> Unit = { },
    private val itemEnabledClickListener: (Source) -> Unit = { }
) : ListAdapter<Source, SourcesAdapter.ViewHolder>(Callback()) {
    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val source = currentList[position]
        with(ItemSourceBinding.bind(holder.itemView)) {
            lblName.text = source.name
            lblUrl.text = source.url
            btnDelete.setOnClickListener { itemDeleteListener(source) }
            container.setOnClickListener { itemClickListener(source) }
            chkEnabled.isChecked = source.isEnabled
            chkEnabled.setOnCheckedChangeListener { _, isChecked ->
                source.isEnabled = isChecked
                itemEnabledClickListener(source)
            }
        }
    }

    override fun getItemId(position: Int): Long = currentList[position].id!!

    inner class ViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_source))

    private class Callback : DiffUtil.ItemCallback<Source>() {
        override fun areItemsTheSame(oldItem: Source, newItem: Source): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Source, newItem: Source): Boolean {
            return oldItem.name == newItem.name &&
                    oldItem.url == newItem.url
        }
    }
}