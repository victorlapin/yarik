package com.victorlapin.yarik.ui.adapters

import android.content.Context
import android.graphics.Color
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.ListPreloader
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.victorlapin.yarik.R
import com.victorlapin.yarik.databinding.ItemFeedBinding
import com.victorlapin.yarik.inflate
import com.victorlapin.yarik.model.database.entity.ArticleWithSource
import com.victorlapin.yarik.ui.GlideApp
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class FeedAdapter(
    private val context: Context,
    private val showImages: Boolean,
    private val itemClickListener: (ArticleWithSource, View) -> Unit = { _, _ -> },
    private val itemLongClickListener: (ArticleWithSource) -> Boolean = { false }
) : ListAdapter<ArticleWithSource, FeedAdapter.ViewHolder>(Callback()) {
    private val formatter = SimpleDateFormat.getDateTimeInstance(
        DateFormat.LONG,
        DateFormat.SHORT
    )

    val preloadModelProvider by lazy { FeedPreloadModelProvider() }

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = currentList[position]
        with(ItemFeedBinding.bind(holder.itemView)) {
            container.setOnClickListener { itemClickListener(item, viewBackground) }
            container.setOnLongClickListener { itemLongClickListener(item) }
            lblSource.text = item.sourceName
            if (item.article.isNew) {
                val newText = context.getString(R.string.new_mark)
                val spannable = SpannableString("$newText ${item.article.title}")
                spannable.setSpan(
                    ForegroundColorSpan(Color.RED), 0, newText.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                lblTitle.text = spannable
            } else {
                lblTitle.text = item.article.title
            }
            lblDescription.text = Html.fromHtml(
                item.article.description,
                Html.FROM_HTML_MODE_COMPACT
            ).toString()
            lblCategories.text = item.tags.joinToString(", ")
            lblAuthor.text = item.article.author
            lblTime.text = formatter.format(Date(item.article.pubDate!!))

            imgHero.isVisible = showImages
            if (showImages) {
                GlideApp.with(root)
                    .load(item.article.image)
                    .thumbnail(0.1f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgHero)
            }
        }
    }

    override fun getItemId(position: Int): Long = currentList[position].article.id!!

    fun getFirstUnreadPosition(): Int = currentList.indexOfFirst { it.article.isNew }

    inner class ViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_feed))

    private class Callback : DiffUtil.ItemCallback<ArticleWithSource>() {
        override fun areItemsTheSame(
            oldItem: ArticleWithSource,
            newItem: ArticleWithSource
        ): Boolean =
            oldItem.article.id == newItem.article.id

        override fun areContentsTheSame(
            oldItem: ArticleWithSource,
            newItem: ArticleWithSource
        ): Boolean {
            return oldItem.article.sourceId == newItem.article.sourceId &&
                    oldItem.article.title == newItem.article.title
        }
    }

    inner class FeedPreloadModelProvider : ListPreloader.PreloadModelProvider<ArticleWithSource> {
        override fun getPreloadItems(position: Int): MutableList<ArticleWithSource> {
            val model = currentList[position]
            return if (model.article.link.isNullOrBlank()) {
                mutableListOf()
            } else {
                mutableListOf(model)
            }
        }

        override fun getPreloadRequestBuilder(item: ArticleWithSource): RequestBuilder<*> =
            GlideApp.with(context)
                .load(item.article.image)
                .thumbnail(0.1f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
    }
}