package com.victorlapin.yarik.ui.fragments

import android.os.Bundle
import android.text.InputType
import android.text.method.DigitsKeyListener
import android.view.View
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.navigation.fragment.findNavController
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.victorlapin.yarik.BuildConfig
import com.victorlapin.yarik.R
import com.victorlapin.yarik.addSystemTopPadding
import com.victorlapin.yarik.databinding.DialogInputBinding
import com.victorlapin.yarik.model.interactor.FirebaseDbInteractor
import com.victorlapin.yarik.model.interactor.WorkerInteractor
import com.victorlapin.yarik.provider.ResourcesProvider
import com.victorlapin.yarik.provider.SettingsProvider
import com.victorlapin.yarik.ui.activities.BaseActivity
import com.victorlapin.yarik.ui.preferences.SignOutPreference
import org.koin.android.ext.android.inject
import timber.log.Timber

class SettingsGlobalFragment : PreferenceFragmentCompat() {
    private val settings by inject<SettingsProvider>()
    private val resources by inject<ResourcesProvider>()
    private val workerInteractor by inject<WorkerInteractor>()
    private val firebaseInteractor by inject<FirebaseDbInteractor>()

    private var isSignedIn = false
    private lateinit var accountPref: Preference
    private lateinit var signOutPref: SignOutPreference

    private val launcher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(it.data)
            try {
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                Timber.w(e, "Google sign in failed")
                setAccountSummary()
            }
        }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_global)

        val themePreference = findPreference<Preference>(SettingsProvider.KEY_THEME)
        val entries = listOf(
            resources.getString(R.string.theme_light),
            resources.getString(R.string.theme_dark),
            resources.getString(R.string.theme_black)
        )
        val values = listOf(
            R.style.AppTheme_Light_Pixel.toString(),
            R.style.AppTheme_Dark_Pixel.toString(),
            R.style.AppTheme_Black_Pixel.toString()
        )
        val initialSelection = values.indexOf(settings.theme.toString())

        themePreference!!.setOnPreferenceClickListener {
            MaterialDialog(requireContext()).show {
                lifecycleOwner(this@SettingsGlobalFragment)
                title(R.string.pref_title_theme)
                listItemsSingleChoice(
                    items = entries,
                    initialSelection = initialSelection,
                    waitForPositiveButton = false
                ) { dialog, index, _ ->
                    dialog.dismiss()
                    val theme = Integer.valueOf(values[index])
                    settings.theme = theme
                    (activity as BaseActivity).updateTheme(theme)
                }
                negativeButton(android.R.string.cancel)
            }
            true
        }

        val daysToKeepPreference = findPreference<Preference>(SettingsProvider.KEY_DAYS_TO_KEEP)
        daysToKeepPreference!!.summary =
            resources.getString(R.string.pref_days_to_keep_summary, settings.daysToKeep)
        daysToKeepPreference.setOnPreferenceClickListener { pref ->
            val dialog = MaterialDialog(requireContext())
                .lifecycleOwner(this@SettingsGlobalFragment)
                .title(R.string.pref_days_to_keep_title)
                .customView(viewRes = R.layout.dialog_input, scrollable = true)
                .negativeButton(android.R.string.cancel)
                .positiveButton(android.R.string.ok) { dialog ->
                    try {
                        val view = dialog.getCustomView()
                        with(DialogInputBinding.bind(view)) {
                            val i = edtInput.text.toString().toInt()
                            if (i > 0) {
                                settings.daysToKeep = i
                                pref.summary = resources.getString(
                                    R.string.pref_days_to_keep_summary,
                                    i
                                )
                            }
                        }
                    } catch (ignore: Exception) {
                    }
                }

            with(DialogInputBinding.bind(dialog.getCustomView())) {
                edtInput.setText(
                    settings.daysToKeep.toString(),
                    TextView.BufferType.EDITABLE
                )
                edtInput.inputType = InputType.TYPE_CLASS_NUMBER
                edtInput.keyListener = DigitsKeyListener.getInstance("0123456789")
            }
            dialog.show()
            true
        }

        val downloadPreference =
            findPreference<SwitchPreference>(SettingsProvider.KEY_DOWNLOAD_NEW_CONTENT)
        downloadPreference!!.setOnPreferenceChangeListener { _, newValue ->
            val b = newValue.toString().toBoolean()
            if (b) {
                workerInteractor.scheduleDownloadContentWorker()
            } else {
                workerInteractor.cancelDownloadContentWorker()
            }
            true
        }

        val downloadWifiPreference =
            findPreference<SwitchPreference>(SettingsProvider.KEY_DOWNLOAD_WIFI_ONLY)
        downloadWifiPreference!!.setOnPreferenceChangeListener { _, newValue ->
            val b = newValue.toString().toBoolean()
            settings.downloadViaWifiOnly = b
            workerInteractor.scheduleDownloadContentWorker()
            true
        }

        signOutPref = findPreference(SettingsProvider.KEY_SIGN_OUT)!!
        signOutPref.setOnPreferenceClickListener {
            MaterialDialog(requireContext()).show {
                lifecycleOwner(this@SettingsGlobalFragment)
                title(R.string.action_sign_out)
                message(R.string.sign_out_confirmation)
                negativeButton(android.R.string.cancel)
                positiveButton(R.string.action_sign_out_short) {
                    FirebaseAuth.getInstance().signOut()
                    setAccountSummary()
                    firebaseInteractor.setUser(null)
                }
            }
            true
        }

        accountPref = findPreference(SettingsProvider.KEY_ACCOUNT)!!
        setAccountSummary()
        accountPref.setOnPreferenceClickListener {
            if (!isSignedIn) {
                startSignIn()
            }
            true
        }

        findPreference<Preference>(SettingsProvider.KEY_ABOUT)!!.setOnPreferenceClickListener {
            val action = SettingsGlobalFragmentDirections
                .actionFragmentSettingsToFragmentAbout()
            findNavController().navigate(action)
            true
        }
    }

    private fun setAccountSummary() {
        val user = FirebaseAuth.getInstance().currentUser
        accountPref.summary = if (user != null)
            user.email else resources.getString(R.string.pref_account_summary_off)
        isSignedIn = user != null
        signOutPref.isVisible = isSignedIn
    }

    private fun startSignIn() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(BuildConfig.AuthApiKey)
            .requestEmail()
            .build()
        val client = GoogleSignIn.getClient(requireActivity(), gso)
        launcher.launch(client.signInIntent)
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        FirebaseAuth.getInstance().signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    Timber.d("signInWithCredential:success")
                    setAccountSummary()
                    firebaseInteractor.setUser(task.result!!.user)
                    workerInteractor.scheduleDeferredSyncWorker()
                } else {
                    Timber.w(task.exception, "signInWithCredential:failure")
                    setAccountSummary()
                    firebaseInteractor.setUser(null)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.addSystemTopPadding()
    }
}