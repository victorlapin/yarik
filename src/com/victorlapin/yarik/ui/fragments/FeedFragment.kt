package com.victorlapin.yarik.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.ActivityNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader
import com.bumptech.glide.util.ViewPreloadSizeProvider
import com.victorlapin.yarik.R
import com.victorlapin.yarik.addSystemTopPadding
import com.victorlapin.yarik.databinding.DialogBlacklistBinding
import com.victorlapin.yarik.databinding.FragmentFeedBinding
import com.victorlapin.yarik.model.database.entity.ArticleWithSource
import com.victorlapin.yarik.model.database.entity.BlacklistItem
import com.victorlapin.yarik.provider.ServicesProvider
import com.victorlapin.yarik.provider.SettingsProvider
import com.victorlapin.yarik.ui.GlideApp
import com.victorlapin.yarik.ui.adapters.FeedAdapter
import com.victorlapin.yarik.viewmodel.FeedViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class FeedFragment : Fragment(), Scrollable,
    SwipeRefreshLayout.OnRefreshListener {

    private var _binding: FragmentFeedBinding? = null
    private val binding get() = _binding!!
    private val viewModel by sharedViewModel<FeedViewModel>()
    private val services by inject<ServicesProvider>()
    private val settings by inject<SettingsProvider>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFeedBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            list.addSystemTopPadding()

            val feedAdapter = FeedAdapter(
                context = requireContext(),
                showImages = settings.showImages,
                itemClickListener = { data, background ->
                    openArticle(data, background)
                },
                itemLongClickListener = { showBlacklistDialog(it); true }
            )
            list.apply {
                layoutManager = object : LinearLayoutManager(context) {
                    override fun getExtraLayoutSpace(state: RecyclerView.State?): Int {
                        // return screen height for smooth scrolling
                        return services.getScreenHeight()
                    }
                }
                itemAnimator = DefaultItemAnimator()
                setHasFixedSize(true)
                adapter = feedAdapter

                if (settings.showImages) {
                    val sizeProvider = ViewPreloadSizeProvider<ArticleWithSource>()
                    val preloader = RecyclerViewPreloader<ArticleWithSource>(
                        GlideApp.with(this),
                        feedAdapter.preloadModelProvider,
                        sizeProvider,
                        5
                    )
                    addOnScrollListener(preloader)
                    setItemViewCacheSize(0)
                }

                addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)
                        var position =
                            (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                        var item = (recyclerView.adapter as FeedAdapter).currentList[position]
                        if (item.article.isNew) {
                            item.article.isNew = false
                            viewModel.updateArticle(item.article)
                        }

                        position =
                            (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                        item = (recyclerView.adapter as FeedAdapter).currentList[position]
                        if (item.article.isNew) {
                            item.article.isNew = false
                            viewModel.updateArticle(item.article)
                        }
                    }
                })
            }

            val typedValue = TypedValue()
            val theme = requireContext().theme
            theme.resolveAttribute(R.attr.progress_background_color, typedValue, true)
            swipeRefresh.setProgressBackgroundColorSchemeColor(typedValue.data)
            theme.resolveAttribute(R.attr.progress_accent_color, typedValue, true)
            swipeRefresh.setColorSchemeColors(typedValue.data)
            swipeRefresh.setOnRefreshListener(this@FeedFragment)

            viewModel.successData.observe(viewLifecycleOwner) {
                list.post {
                    (list.adapter as FeedAdapter).submitList(it)
                }
            }
            viewModel.infoData.observe(viewLifecycleOwner) { info ->
                info?.let {
                    snackbar.show(
                        messageId = it.messageId,
                        messageText = it.message,
                        formatArg = it.formatArg,
                        longDuration = true
                    )
                    viewModel.consumeInfo()
                }
            }
            viewModel.loadingData.observe(viewLifecycleOwner) {
                swipeRefresh.isRefreshing = it
            }
        }
    }

    override fun onRefresh() = viewModel.refreshFeed()

    @SuppressLint("CutPasteId")
    private fun showBlacklistDialog(item: ArticleWithSource) {
        val dialog = MaterialDialog(requireContext())
            .lifecycleOwner(this)
            .title(R.string.feed_blacklist)
            .customView(viewRes = R.layout.dialog_blacklist, scrollable = true)
            .noAutoDismiss()
            .cancelOnTouchOutside(true)
            .negativeButton(res = android.R.string.cancel) { dialog ->
                dialog.dismiss()
            }
            .positiveButton(res = android.R.string.ok) { dialog ->
                val view = dialog.getCustomView()
                with(DialogBlacklistBinding.bind(view)) {
                    val blacklist = ArrayList<BlacklistItem>()
                    if (chkAuthor.isChecked) {
                        blacklist.add(
                            BlacklistItem(
                                value = chkAuthor.text.toString(),
                                type = BlacklistItem.TYPE_AUTHOR
                            )
                        )
                    }
                    (0 until containerTags.childCount).forEach {
                        val tagView = containerTags.getChildAt(it)
                        val chkTag = tagView.findViewById<CheckBox>(R.id.chk_tag)
                        if (chkTag.isChecked) {
                            blacklist.add(
                                BlacklistItem(
                                    value = chkTag.text.toString(),
                                    type = BlacklistItem.TYPE_TAG
                                )
                            )
                        }
                    }

                    if (blacklist.isNotEmpty()) {
                        viewModel.onBlacklistAdded(blacklist)
                    }
                }
                dialog.dismiss()
            }

        val view = dialog.getCustomView()
        with(DialogBlacklistBinding.bind(view)) {
            chkAuthor.text = item.article.author
            if (item.tags.isNullOrEmpty()) {
                lblTags.isVisible = false
            } else {
                item.tags.forEach { tag ->
                    val tagView = LayoutInflater.from(requireContext()).inflate(
                        R.layout.dialog_blacklist_tag,
                        containerTags,
                        false
                    )
                    tagView.findViewById<CheckBox>(R.id.chk_tag).text = tag.value
                    containerTags.addView(tagView)
                }
            }
        }
        dialog.show()
    }

    override fun scrollToTop() {
        with(binding) {
            if ((list.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition() == 0) {
                val firstUnread = (list.adapter as FeedAdapter).getFirstUnreadPosition()
                list.scrollToPosition(if (firstUnread > -1) firstUnread else 0)
            } else {
                list.scrollToPosition(0)
            }
        }
    }

    private fun openArticle(data: ArticleWithSource, background: View) {
        val action = FeedFragmentDirections
            .actionFragmentFeedToActivityArticle(data.article)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            requireActivity(),
            androidx.core.util.Pair(background, getString(R.string.transition_container))
        )
        findNavController().navigate(action, ActivityNavigatorExtras(options))
    }
}