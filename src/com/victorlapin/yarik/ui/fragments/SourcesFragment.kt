package com.victorlapin.yarik.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.interpolator.view.animation.FastOutLinearInInterpolator
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Fade
import androidx.transition.TransitionManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.victorlapin.yarik.R
import com.victorlapin.yarik.addSystemTopPadding
import com.victorlapin.yarik.databinding.DialogEditSourceBinding
import com.victorlapin.yarik.databinding.FragmentSourcesBinding
import com.victorlapin.yarik.model.database.entity.Source
import com.victorlapin.yarik.ui.adapters.SourcesAdapter
import com.victorlapin.yarik.viewmodel.SourcesViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class SourcesFragment : Fragment(), Scrollable {

    private var _binding: FragmentSourcesBinding? = null
    private val binding get() = _binding!!
    private val viewModel by viewModel<SourcesViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSourcesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            list.addSystemTopPadding()

            val homeAdapter = SourcesAdapter(
                itemClickListener = { editSource(it) },
                itemDeleteListener = { viewModel.deleteSource(it) },
                itemEnabledClickListener = { viewModel.onSourceEdited(it) }
            )
            list.apply {
                layoutManager = LinearLayoutManager(context)
                itemAnimator = DefaultItemAnimator()
                setHasFixedSize(true)
                adapter = homeAdapter
            }
            fab.setOnClickListener { editSource(null) }

            viewModel.successData.observe(viewLifecycleOwner) {
                list.post {
                    (list.adapter as SourcesAdapter).submitList(it)
                    lblEmpty.isVisible = it.isEmpty()
                }
            }
            viewModel.deletedSourceData.observe(viewLifecycleOwner) { source ->
                source?.let {
                    coordinator.post {
                        val t1 = Fade().setInterpolator(FastOutLinearInInterpolator())
                        TransitionManager.beginDelayedTransition(coordinator, t1)
                        fab.hide()
                        snackbar.show(
                            messageId = R.string.source_deleted,
                            actionId = R.string.action_undo,
                            actionClick = {
                                viewModel.onUndoDelete(source)
                            },
                            dismissListener = {
                                try {
                                    val t2 = Fade().setInterpolator(FastOutLinearInInterpolator())
                                    TransitionManager.beginDelayedTransition(coordinator, t2)
                                    fab.show()
                                    viewModel.onSnackbarDismissed()
                                } catch (ignore: Exception) {
                                }
                            }
                        )
                    }
                }
            }

            list.setOnScrollChangeListener { _: View?, _: Int, scrollY: Int, _: Int, _: Int ->
                if (scrollY == 0) {
                    if (!fab.isExtended) {
                        fab.extend()
                    }
                } else {
                    if (fab.isExtended) {
                        fab.shrink()
                    }
                }
            }
        }
    }

    private fun editSource(source: Source?) {
        val dialog = MaterialDialog(requireContext())
            .lifecycleOwner(this)
            .title(res = if (source != null) R.string.source_edit else R.string.source_new)
            .customView(viewRes = R.layout.dialog_edit_source, scrollable = true)
            .noAutoDismiss()
            .cancelOnTouchOutside(true)
            .negativeButton(res = android.R.string.cancel) { dialog ->
                dialog.dismiss()
            }
            .positiveButton(res = android.R.string.ok) { dialog ->
                val view = dialog.getCustomView()
                with(DialogEditSourceBinding.bind(view)) {
                    val name = edtName.text.toString()
                    val url = edtUrl.text.toString()
                    when {
                        name.isBlank() -> edtName.error =
                            getString(R.string.source_error_empty)
                        url.isBlank() -> edtUrl.error = getString(R.string.source_error_empty)
                        else -> {
                            val newSource = Source(
                                id = source?.id,
                                name = name.trim(),
                                url = url.trim()
                            )
                            viewModel.onSourceEdited(newSource)
                            dialog.dismiss()
                        }
                    }
                }
            }

        val view = dialog.getCustomView()
        with(DialogEditSourceBinding.bind(view)) {
            edtName.setText(source?.name, TextView.BufferType.EDITABLE)
            edtUrl.setText(source?.url, TextView.BufferType.EDITABLE)
            edtUrl.isEnabled = source == null
        }
        dialog.show()
    }

    override fun scrollToTop() = binding.list.scrollToPosition(0)
}