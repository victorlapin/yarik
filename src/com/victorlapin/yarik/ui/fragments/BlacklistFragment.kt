package com.victorlapin.yarik.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.victorlapin.yarik.R
import com.victorlapin.yarik.addSystemTopPadding
import com.victorlapin.yarik.databinding.FragmentBlacklistBinding
import com.victorlapin.yarik.ui.adapters.BlacklistAdapter
import com.victorlapin.yarik.viewmodel.BlacklistViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class BlacklistFragment : Fragment(), Scrollable {

    private var _binding: FragmentBlacklistBinding? = null
    private val binding get() = _binding!!
    private val viewModel by viewModel<BlacklistViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBlacklistBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            list.addSystemTopPadding()

            val blacklistAdapter = BlacklistAdapter(
                itemDeleteListener = { viewModel.removeFromBlacklist(it) }
            )
            list.apply {
                layoutManager = LinearLayoutManager(context)
                itemAnimator = DefaultItemAnimator()
                setHasFixedSize(true)
                adapter = blacklistAdapter
            }

            viewModel.successData.observe(viewLifecycleOwner) {
                list.post {
                    (list.adapter as BlacklistAdapter).submitList(it)
                    lblEmpty.isVisible = it.isEmpty()
                }
            }
            viewModel.deletedItemData.observe(viewLifecycleOwner) { item ->
                item?.let {
                    snackbar.show(
                        messageId = R.string.blacklist_deleted,
                        formatArg = it.value,
                        actionId = R.string.action_undo,
                        actionClick = {
                            viewModel.onUndoDelete(it)
                        },
                        dismissListener = { viewModel.onSnackbarDismissed() }
                    )
                }
            }
        }
    }

    override fun scrollToTop() = binding.list.scrollToPosition(0)
}