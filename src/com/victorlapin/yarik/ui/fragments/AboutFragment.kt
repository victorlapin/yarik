package com.victorlapin.yarik.ui.fragments

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.victorlapin.yarik.R
import com.victorlapin.yarik.databinding.FragmentAboutBinding
import com.victorlapin.yarik.ui.adapters.AboutAdapter
import com.victorlapin.yarik.viewmodel.AboutViewModel
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel

class AboutFragment : Fragment() {

    private var _binding: FragmentAboutBinding? = null
    private val binding get() = _binding!!
    private val viewModel by viewModel<AboutViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAboutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            includes.toolbar.setTitle(R.string.action_about)
            includes.toolbar.setNavigationIcon(R.drawable.arrow_left)
            includes.toolbar.setNavigationOnClickListener { findNavController().navigateUp() }

            val aboutAdapter = AboutAdapter(
                resources = get()
            )
            list.apply {
                when (requireActivity().resources.configuration.orientation) {
                    Configuration.ORIENTATION_LANDSCAPE -> {
                        val lm = GridLayoutManager(context, GRID_COLUMN_COUNT)
                        lm.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                            override fun getSpanSize(position: Int): Int {
                                return when (adapter!!.getItemViewType(position)) {
                                    AboutAdapter.ITEM_TYPE_ITEM -> 1
                                    else -> GRID_COLUMN_COUNT
                                }
                            }
                        }
                        layoutManager = lm
                    }
                    else -> layoutManager = LinearLayoutManager(context)
                }
                itemAnimator = DefaultItemAnimator()
                setHasFixedSize(true)
                adapter = aboutAdapter
            }

            list.post {
                aboutAdapter.setData(viewModel.getData())
            }
        }
    }

    companion object {
        private const val GRID_COLUMN_COUNT = 2
    }
}