package com.victorlapin.yarik.ui.fragments

interface Scrollable {
    fun scrollToTop()
}