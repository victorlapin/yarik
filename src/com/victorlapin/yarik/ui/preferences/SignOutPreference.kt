package com.victorlapin.yarik.ui.preferences

import android.content.Context
import android.util.AttributeSet
import android.widget.Button
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import com.victorlapin.yarik.R

class SignOutPreference @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet,
    defStyleAttr: Int = 0
) : Preference(context, attrs, defStyleAttr) {
    init {
        layoutResource = R.layout.preference_sign_out
    }

    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        super.onBindViewHolder(holder)
        holder.itemView.background = null
        holder.isDividerAllowedAbove = false
        holder.isDividerAllowedBelow = false
        holder.itemView.findViewById<Button>(R.id.btn_sign_out).setOnClickListener {
            onPreferenceClickListener.onPreferenceClick(this)
        }
    }
}