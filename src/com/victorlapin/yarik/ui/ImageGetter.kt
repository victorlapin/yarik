package com.victorlapin.yarik.ui

import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.Html
import android.widget.TextView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import java.lang.ref.WeakReference

class ImageGetter(target: TextView) : Html.ImageGetter {
    private val targetRef: WeakReference<TextView> = WeakReference(target)

    override fun getDrawable(source: String?): Drawable {
        val drawable = DrawablePlaceholder()
        val textView = targetRef.get()
        textView?.let {
            GlideApp.with(it)
                .load(source)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true)
                .into(drawable.target)
        }
        return drawable
    }

    inner class DrawablePlaceholder : BitmapDrawable() {
        private var drawable: Drawable? = null

        val target = object : CustomTarget<Drawable>() {
            override fun onLoadCleared(placeholder: Drawable?) {
                drawable = null
            }

            override fun onResourceReady(
                resource: Drawable,
                transition: Transition<in Drawable>?
            ) {
                setDrawable(resource)
            }

            override fun onLoadStarted(placeholder: Drawable?) {
                placeholder?.let { setDrawable(it) }
            }

            override fun onLoadFailed(errorDrawable: Drawable?) {
                errorDrawable?.let { setDrawable(it) }
            }
        }

        override fun draw(canvas: Canvas) {
            drawable?.draw(canvas)
        }

        private fun setDrawable(resource: Drawable) {
            drawable = resource
            val width = resource.intrinsicWidth
            val height = resource.intrinsicHeight
            val scale = getScale(resource)
            val bounds = Rect(0, 0, (width * scale).toInt(), (height * scale).toInt())

            drawable!!.bounds = bounds
            setBounds(bounds)

            val textView = targetRef.get()
            textView?.let {
                it.invalidate()
                it.text = it.text
            }
        }

        private fun getScale(resource: Drawable): Float {
            val textView = targetRef.get()
            return if (textView != null) {
                val maxWidth: Float =
                    (textView.width - textView.paddingStart - textView.paddingEnd).toFloat()
                (maxWidth / resource.intrinsicWidth)
            } else 1F
        }
    }
}