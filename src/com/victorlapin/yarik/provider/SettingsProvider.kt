package com.victorlapin.yarik.provider

import android.content.Context
import androidx.preference.PreferenceManager
import com.victorlapin.yarik.R

class SettingsProvider(context: Context) {
    companion object {
        const val KEY_THEME = "interface_theme"
        const val KEY_USE_CUSTOM_TABS = "use_custom_tabs"
        const val KEY_DAYS_TO_KEEP = "days_to_keep"
        const val KEY_DOWNLOAD_NEW_CONTENT = "download_new_content"
        const val KEY_DOWNLOAD_WIFI_ONLY = "download_wifi_only"
        const val KEY_ACCOUNT = "account"
        const val KEY_SIGN_OUT = "sign_out"
        const val KEY_SHOW_IMAGES = "show_images"
        const val KEY_ABOUT = "open_about"
        const val KEY_FONT_SIZE = "font_size"
        const val KEY_ARTICLE_PRELOAD = "article_preload"
        const val KEY_OPTIMIZED_FETCH = "optimized_fetch"

        const val FONT_SIZE_SMALL = 0
        const val FONT_SIZE_MEDIUM = 1
        const val FONT_SIZE_LARGE = 2
    }

    private val prefs = PreferenceManager.getDefaultSharedPreferences(context)

    var theme: Int
        get() = when (val id = prefs.getInt(KEY_THEME, R.style.AppTheme_Light_Pixel)) {
            R.style.AppTheme_Light_Pixel,
            R.style.AppTheme_Dark_Pixel,
            R.style.AppTheme_Black_Pixel -> id
            else -> R.style.AppTheme_Light_Pixel
        }
        set(theme) = prefs.edit().putInt(KEY_THEME, theme).apply()

    val useCustomTabs: Boolean
        get() = prefs.getBoolean(KEY_USE_CUSTOM_TABS, true)

    var daysToKeep: Int
        get() = prefs.getInt(KEY_DAYS_TO_KEEP, 1)
        set(value) = prefs.edit().putInt(KEY_DAYS_TO_KEEP, value).apply()

    var downloadViaWifiOnly: Boolean
        get() = prefs.getBoolean(KEY_DOWNLOAD_WIFI_ONLY, true)
        set(value) = prefs.edit().putBoolean(KEY_DOWNLOAD_WIFI_ONLY, value).apply()

    val showImages: Boolean
        get() = prefs.getBoolean(KEY_SHOW_IMAGES, true)

    var fontSize: Int
        get() = prefs.getInt(KEY_FONT_SIZE, FONT_SIZE_MEDIUM)
        set(size) = prefs.edit().putInt(KEY_FONT_SIZE, size).apply()

    val preloadArticles: Boolean
        get() = prefs.getBoolean(KEY_ARTICLE_PRELOAD, true)

    val useOptimizedFetch: Boolean
        get() = prefs.getBoolean(KEY_OPTIMIZED_FETCH, true)
}