package com.victorlapin.yarik.provider

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Binder
import android.os.IBinder
import androidx.annotation.ColorInt
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsClient
import androidx.browser.customtabs.CustomTabsIntent
import androidx.browser.customtabs.CustomTabsSession

class CustomTabsProvider(
    private val packageName: String
) {
    private var client: CustomTabsClient? = null
    private var session: CustomTabsSession? = null

    fun setClient(client: CustomTabsClient) {
        client.warmup(0)
        this.client = client
    }

    fun cleanup() {
        session = null
        client = null
    }

    private fun getSession(): CustomTabsSession? {
        if (session == null) {
            session = client?.newSession(null)
        }
        return session
    }

    fun preloadUrl(url: String) {
        getSession()?.mayLaunchUrl(Uri.parse(url), null, null)
    }

    private fun getIntent(
        @ColorInt toolbarColor: Int,
        isDarkMode: Boolean,
        packageName: String
    ): CustomTabsIntent {
        val params = CustomTabColorSchemeParams.Builder()
            .setToolbarColor(toolbarColor)
            .build()
        val cti = CustomTabsIntent.Builder(getSession())
            .setShareState(CustomTabsIntent.SHARE_STATE_ON)
            .setDefaultColorSchemeParams(params)
            .setColorScheme(
                if (isDarkMode) CustomTabsIntent.COLOR_SCHEME_DARK else
                    CustomTabsIntent.COLOR_SCHEME_LIGHT
            )
            .build()
        addKeepAlive(cti.intent)
        addReferrer(cti.intent, packageName)
        return cti
    }

    private fun addKeepAlive(baseIntent: Intent) {
        val keepAliveIntent = Intent().setClassName(
            packageName,
            KeepAliveService::class.java.canonicalName!!
        )
        baseIntent.putExtra(EXTRA_CUSTOM_TABS_KEEP_ALIVE, keepAliveIntent)
    }

    private fun addReferrer(baseIntent: Intent, packageName: String) {
        baseIntent.putExtra(
            Intent.EXTRA_REFERRER,
            Uri.parse("android-app://$packageName")
        )
    }

    private fun isConnected(): Boolean = client != null

    @SuppressLint("Registered")
    class KeepAliveService : Service() {
        override fun onBind(intent: Intent?): IBinder? = binder

        companion object {
            private val binder = Binder()
        }
    }

    fun openUrl(
        context: Context,
        url: String,
        @ColorInt toolbarColor: Int,
        useCustomTab: Boolean,
        isDarkMode: Boolean
    ) {
        if (useCustomTab && isConnected()) {
            getIntent(toolbarColor, isDarkMode, context.packageName).launchUrl(
                context,
                Uri.parse(url)
            )
        } else {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        }
    }

    companion object {
        private const val EXTRA_CUSTOM_TABS_KEEP_ALIVE =
            "android.support.customtabs.extra.KEEP_ALIVE"
    }
}