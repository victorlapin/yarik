package com.victorlapin.yarik.provider

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

class ResourcesProvider(private val context: Context) {
    fun getString(@StringRes id: Int, arg: Any? = null): String = context.getString(id, arg)

    fun getDrawable(@DrawableRes id: Int): Drawable? = context.getDrawable(id)

    fun getColor(@ColorRes id: Int): Int =
        ContextCompat.getColor(context, id)
}