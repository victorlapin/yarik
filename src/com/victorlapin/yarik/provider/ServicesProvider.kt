package com.victorlapin.yarik.provider

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.provider.Settings
import android.view.WindowManager
import androidx.core.app.NotificationCompat
import com.victorlapin.yarik.R
import com.victorlapin.yarik.ui.activities.MainActivity

class ServicesProvider(
    private val context: Context,
    private val resources: ResourcesProvider
) {
    private val mSelfPendingIntent = PendingIntent.getActivity(
        context, 0,
        Intent(context, MainActivity::class.java), 0
    )

    private val notificationManager by lazy {
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    fun createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val manager = notificationManager

            val channelId = resources.getString(R.string.channel_new_articles_id)
            if (manager.getNotificationChannel(channelId) == null) {
                with(
                    NotificationChannel(
                        channelId,
                        resources.getString(R.string.channel_new_articles_title),
                        NotificationManager.IMPORTANCE_DEFAULT
                    )
                ) {
                    setShowBadge(true)
                    enableVibration(true)
                    enableLights(true)
                    manager.createNotificationChannel(this)
                }
            }
        }
    }

    fun showNewArticlesNotification(count: Int) {
        val builder = NotificationCompat.Builder(
            context,
            resources.getString(R.string.channel_new_articles_id)
        )
            .setSmallIcon(R.drawable.book_open_variant)
            .setContentTitle(resources.getString(R.string.notification_new_articles_title))
            .setContentText(resources.getString(R.string.notification_new_articles_text, count))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setShowWhen(true)
            .setDefaults(Notification.DEFAULT_ALL)
            .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
            .setContentIntent(mSelfPendingIntent)
            .setAutoCancel(true)
            .setStyle(NotificationCompat.BigTextStyle())
            .setColor(resources.getColor(R.color.accent))

        notificationManager.notify(UPDATES_NOTIFICATION_ID, builder.build())
    }

    private val windowManager by lazy {
        context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    }

    fun getScreenHeight(): Int = windowManager.currentWindowMetrics.bounds.height()

    val selfPackageName: String = context.packageName

    val packageManager: PackageManager by lazy {
        context.packageManager
    }


    companion object {
        private const val UPDATES_NOTIFICATION_ID = 500
    }
}