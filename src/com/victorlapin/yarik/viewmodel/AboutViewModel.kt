package com.victorlapin.yarik.viewmodel

import androidx.lifecycle.ViewModel
import com.victorlapin.yarik.model.interactor.AboutInteractor
import com.victorlapin.yarik.model.repository.AboutRepository

class AboutViewModel(
    private val interactor: AboutInteractor
) : ViewModel() {
    fun getData(): List<AboutRepository.ListItem> = interactor.getData()
}