package com.victorlapin.yarik.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.victorlapin.yarik.R
import com.victorlapin.yarik.model.NoSourcesException
import com.victorlapin.yarik.model.SnackbarInfo
import com.victorlapin.yarik.model.database.entity.Article
import com.victorlapin.yarik.model.database.entity.ArticleWithSource
import com.victorlapin.yarik.model.database.entity.BlacklistItem
import com.victorlapin.yarik.model.interactor.ArticlesInteractor
import com.victorlapin.yarik.model.interactor.BlacklistInteractor
import com.victorlapin.yarik.model.interactor.FeedInteractor
import com.victorlapin.yarik.provider.SettingsProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

class FeedViewModel(
    private val articlesInteractor: ArticlesInteractor,
    private val feedInteractor: FeedInteractor,
    private val settings: SettingsProvider,
    private val blacklistInteractor: BlacklistInteractor
) : ViewModel() {
    val successData: LiveData<List<ArticleWithSource>> = articlesInteractor.getArticles().switchMap { articles ->
        val filtered = articles.filter {
            it.article.pubDate != null
                    && it.article.pubDate >= System.currentTimeMillis() - TimeUnit.DAYS.toMillis(
                settings.daysToKeep.toLong()
            )
        }
        MutableLiveData(filtered)
    }

    private val mutableInfoData = MutableLiveData<SnackbarInfo?>()
    val infoData: LiveData<SnackbarInfo?> = mutableInfoData

    private val mutableLoadingData = MutableLiveData<Boolean>()
    val loadingData: LiveData<Boolean> = mutableLoadingData

    fun consumeInfo() = mutableInfoData.postValue(null)

    fun refreshFeed() {
        viewModelScope.launch((Dispatchers.IO)) {
            mutableLoadingData.postValue(true)
            try {
                feedInteractor.refreshFeed(this)
            } catch (e: Exception) {
                val info = SnackbarInfo(
                    messageId = if (e is NoSourcesException) R.string.feed_no_sources_error
                    else R.string.feed_refresh_error
                )
                mutableInfoData.postValue(info)
            } finally {
                mutableLoadingData.postValue(false)
            }
        }
    }

    fun onBlacklistAdded(items: List<BlacklistItem>) {
        viewModelScope.launch((Dispatchers.IO)) {
            blacklistInteractor.insertBlacklistItems(items)
        }
    }

    fun updateArticle(article: Article) {
        viewModelScope.launch((Dispatchers.IO)) {
            articlesInteractor.updateArticle(article)
        }
    }
}