package com.victorlapin.yarik.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.google.firebase.auth.FirebaseAuth
import com.victorlapin.yarik.model.interactor.ArticlesInteractor
import com.victorlapin.yarik.model.interactor.FirebaseDbInteractor
import com.victorlapin.yarik.model.interactor.WorkerInteractor
import com.victorlapin.yarik.provider.SettingsProvider
import java.util.concurrent.TimeUnit

class MainViewModel(
    private val workerInteractor: WorkerInteractor,
    private val firebaseInteractor: FirebaseDbInteractor,
    private val settings: SettingsProvider,
    articlesInteractor: ArticlesInteractor
) : ViewModel() {
    val newArticlesCountData: LiveData<Int> =
        articlesInteractor.getUnreadArticles().switchMap { articles ->
            val filtered = articles.filter {
                it.pubDate != null
                        && it.pubDate >= System.currentTimeMillis() - TimeUnit.DAYS.toMillis(
                    settings.daysToKeep.toLong()
                )
            }
            MutableLiveData(filtered.size)
        }

    fun onStarted() {
        firebaseInteractor.setUser(FirebaseAuth.getInstance().currentUser)
        workerInteractor.scheduleDatabaseHygieneWorker()
    }

    override fun onCleared() {
        firebaseInteractor.setUser(null)
        super.onCleared()
    }
}