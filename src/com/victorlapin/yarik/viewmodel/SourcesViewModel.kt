package com.victorlapin.yarik.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.victorlapin.yarik.model.database.entity.Source
import com.victorlapin.yarik.model.interactor.SourcesInteractor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SourcesViewModel(
    private val interactor: SourcesInteractor
) : ViewModel() {
    val successData = interactor.getSources()

    private val mutableDeletedSourceData = MutableLiveData<Source?>()
    val deletedSourceData: LiveData<Source?> = mutableDeletedSourceData

    fun deleteSource(source: Source) {
        viewModelScope.launch((Dispatchers.IO)) {
            interactor.deleteSource(source)
            mutableDeletedSourceData.postValue(source)
        }
    }

    fun onUndoDelete(source: Source) {
        viewModelScope.launch((Dispatchers.IO)) {
            interactor.insertSource(source)
        }
    }

    fun onSourceEdited(source: Source) {
        viewModelScope.launch((Dispatchers.IO)) {
            if (source.id == null) {
                interactor.insertSource(source)
            } else {
                interactor.updateSource(source)
            }
        }
    }

    fun onSnackbarDismissed() = mutableDeletedSourceData.postValue(null)
}