package com.victorlapin.yarik.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.victorlapin.yarik.model.database.entity.Source
import com.victorlapin.yarik.model.interactor.SourcesInteractor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShareReceiverViewModel(
    private val interactor: SourcesInteractor
) : ViewModel() {
    fun onSourceAdded(source: Source) {
        viewModelScope.launch(Dispatchers.IO) {
            interactor.insertSource(source)
        }
    }
}