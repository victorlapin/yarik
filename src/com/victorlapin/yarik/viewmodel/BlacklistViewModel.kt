package com.victorlapin.yarik.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.victorlapin.yarik.model.database.entity.BlacklistItem
import com.victorlapin.yarik.model.interactor.BlacklistInteractor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BlacklistViewModel(
    private val interactor: BlacklistInteractor
) : ViewModel() {
    val successData = interactor.getBlacklist()

    private val mutableDeletedItemData = MutableLiveData<BlacklistItem?>()
    val deletedItemData: LiveData<BlacklistItem?> = mutableDeletedItemData

    fun removeFromBlacklist(item: BlacklistItem) {
        viewModelScope.launch((Dispatchers.IO)) {
            interactor.deleteBlacklistItem(item)
            mutableDeletedItemData.postValue(item)
        }
    }

    fun onUndoDelete(item: BlacklistItem) {
        viewModelScope.launch((Dispatchers.IO)) {
            interactor.insertBlacklistItem(item)
        }
    }

    fun onSnackbarDismissed() = mutableDeletedItemData.postValue(null)
}